package com.trontransportation.jaketbustron.activity.home.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.orders.OrderBusActivity
import com.trontransportation.jaketbustron.activity.schedule.ScheduleInfoActivity
import com.trontransportation.jaketbustron.apis.ApiInterface
import com.trontransportation.jaketbustron.models.*
import com.trontransportation.jaketbustron.utilities.AdapterUtil
import com.trontransportation.jaketbustron.utilities.service
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_banner.view.*
import kotlinx.android.synthetic.main.item_feature_home.view.*
import kotlinx.android.synthetic.main.item_promo.view.*

class HomeFragment : Fragment() {

    private lateinit var featureHomeAdapter: AdapterUtil<ItemFeatureHome>
    private lateinit var bannerHomeAdapter: AdapterUtil<GetFeatures.Data>
    private lateinit var promoHomeAdapter: AdapterUtil<GetExperiences.Data>
    private lateinit var rootActivity: HomeActivity
    private var isLoadedbanner = false
    private var isLoadedExp = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Adapter
        featureHomeAdapter = AdapterUtil(R.layout.item_feature_home, listOf(
            ItemFeatureHome(R.drawable.ic_order_bus, R.string.label_order_bus),
            ItemFeatureHome(R.drawable.ic_info_schedule, R.string.label_info_schedule)
        ),
            {itemView, item ->
                Glide.with(itemView)
                    .load(item.icon)
                    .into(itemView.item_feature_home_icon)
                itemView.item_feature_home_label.text = getString(item.label)
            },
            {position, _ ->
                if (position == 0) {
                    requireActivity()
                        .startActivity(Intent(requireContext(), OrderBusActivity::class.java))
                    requireActivity().finish()
                } else {
                    requireActivity()
                        .startActivity(Intent(requireContext(), ScheduleInfoActivity::class.java))
                }
            })

        bannerHomeAdapter = AdapterUtil(R.layout.item_banner, listOf(),
            {itemView, item ->
                Glide.with(itemView)
                    .load(item.cover.uri)
                    .into(itemView.item_banner_img)
            },
            {_, _->})

        promoHomeAdapter = AdapterUtil(R.layout.item_promo, listOf(),
            {itemView, item ->
                Glide.with(itemView)
                    .load(item.cover.uri)
                    .into(itemView.item_promo_img)
                itemView.item_promo_title.text = StringBuilder().append(item.title)
                itemView.item_promo_rating.text = StringBuilder().append(item.rating)
            },
            {_, _->})

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private fun checkIsAlreadyLoaded() {
        if (!isLoadedExp && !isLoadedbanner) rootActivity.setLoadedDataStatus(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rootActivity.setLoadedDataStatus(true)

        // Setup Feature Home
        list_feature_home.layoutManager = GridLayoutManager(requireContext(), 2)
        list_feature_home.adapter = featureHomeAdapter

        // Setup Banner Home
        list_banner_home.layoutManager = LinearLayoutManager(requireContext(),
            LinearLayoutManager.HORIZONTAL, false)
        list_banner_home.adapter = bannerHomeAdapter

        // Setup Promo Home
        list_promo_home.layoutManager = LinearLayoutManager(requireContext(),
            LinearLayoutManager.HORIZONTAL, false)
        list_promo_home.adapter = promoHomeAdapter

        // Load Banner
        this.loadBanner()

        // Load Experience
        this.loadExperience()
    }

    private fun loadBanner() {
        isLoadedbanner = true
        getFeatures(service!!, 0, {
            if (it.code == 200) {
                bannerHomeAdapter.data = it.data
            } else {
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            }
            isLoadedbanner = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            isLoadedbanner = false
            checkIsAlreadyLoaded()
        })
    }

    private fun loadExperience() {
        isLoadedExp = true
        getExperiences(service!!, 0, {
            if (it.code == 200) {
                promoHomeAdapter.data = it.data
            } else {
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            }
            isLoadedExp = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            isLoadedExp = false
            checkIsAlreadyLoaded()
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(rootActivity: HomeActivity) = HomeFragment().apply {
            this.rootActivity = rootActivity
        }
    }
}