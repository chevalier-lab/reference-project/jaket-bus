package com.trontransportation.jaketbustron.activity.schedule.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.models.Seat
import com.trontransportation.jaketbustron.models.getSeats
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.fragment_choose_seat.*
import kotlinx.android.synthetic.main.item_choose_seat.view.*

class ChooseSeatFragment : Fragment() {

    private lateinit var listChooseSeatAdapter: AdapterUtil<Seat.Data.ItemSeat>
    internal lateinit var scheduleID: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup List Choose Adapter
        listChooseSeatAdapter = AdapterUtil(R.layout.item_choose_seat, listOf(),
            {itemView, item ->
                if (item.seat != 0 && item.available) {
                    itemView.item_choose_seat_container.setBackgroundColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorWhite)
                    )

                    itemView.item_choose_seat_label.text = if (item.seat < 10) "0${item.seat}"
                    else "${item.seat}"
                    itemView.item_choose_seat_label.setTextColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorTextLight)
                    )
                } else {
                    itemView.item_choose_seat_container.setBackgroundColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorGrayDark)
                    )

                    itemView.item_choose_seat_label.text = if (item.number < 10) "0${item.number}"
                    else "${item.number}"
                    itemView.item_choose_seat_label.setTextColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorGrayDark)
                    )
                }
            },
            { _, _ ->

            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_seat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Setup List Choose Seat
        list_choose_seat.adapter = listChooseSeatAdapter

        btn_submit.visibility = View.GONE

        // Load Seat
        this.loadSeat()
    }

    private fun loadSeat() {
        loader.visibility = View.VISIBLE
        list_choose_seat.visibility = View.GONE
        val json = JsonObject()
        json.addProperty("id_schedule", scheduleID)
        getSeats(service!!, json, {
            if (it.code == 200) {
                if (it.data.status) {
                    list_choose_seat.layoutManager = GridLayoutManager(requireContext(), it.data.data.layout.seat_column)
                    if (it.data.data.seat.size > 0) listChooseSeatAdapter.data = it.data.data.seat
                } else Toast.makeText(requireContext(), it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_choose_seat.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_choose_seat.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(scheduleID: String) =
            ChooseSeatFragment().apply {
                this.scheduleID = scheduleID
            }
    }
}