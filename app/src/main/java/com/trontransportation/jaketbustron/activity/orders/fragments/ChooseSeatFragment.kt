package com.trontransportation.jaketbustron.activity.orders.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.orders.ChooseTicketBusActivity
import com.trontransportation.jaketbustron.activity.orders.ComfirmationOrderBusActivity
import com.trontransportation.jaketbustron.models.Seat
import com.trontransportation.jaketbustron.models.getSeats
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.fragment_choose_seat.*
import kotlinx.android.synthetic.main.item_choose_seat.view.*

class ChooseSeatFragment : Fragment() {

    internal lateinit var rootActivity: Activity
    private lateinit var listChooseSeatAdapter: AdapterUtil<Seat.Data.ItemSeat>
    internal lateinit var scheduleID: String
    private var selectedSeat: MutableList<Int> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup List Choose Adapter
        listChooseSeatAdapter = AdapterUtil(R.layout.item_choose_seat, listOf(),
            {itemView, item ->
                if (checkIsAlreadyBook(item.number)) {
                    itemView.item_choose_seat_container.setBackgroundColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorAccent)
                    )
                    itemView.item_choose_seat_label.setTextColor(
                        ContextCompat
                            .getColor(requireContext(), R.color.colorWhite))

                    itemView.item_choose_seat_label.text = if (item.seat < 10) "0${item.seat}"
                    else "${item.seat}"
                }
                else {
                    if (item.seat != 0) {
                        if (item.available) {
                            itemView.item_choose_seat_container.setBackgroundColor(
                                ContextCompat
                                    .getColor(requireContext(), R.color.colorWhite)
                            )
                        } else {
                            itemView.item_choose_seat_container.setBackgroundColor(
                                ContextCompat
                                    .getColor(requireContext(), R.color.colorGrayDark)
                            )
                        }

                        itemView.item_choose_seat_label.text = if (item.seat < 10) "0${item.seat}"
                        else "${item.seat}"

                        itemView.item_choose_seat_label.setTextColor(
                            ContextCompat
                                .getColor(requireContext(), R.color.colorTextLight)
                        )
                    } else {
                        itemView.item_choose_seat_container.setBackgroundColor(
                            ContextCompat
                                .getColor(requireContext(), R.color.colorGrayDark)
                        )
                        itemView.item_choose_seat_label.text = if (item.seat < 10) "0${item.seat}"
                        else "${item.seat}"
                        itemView.item_choose_seat_label.setTextColor(
                            ContextCompat
                                .getColor(requireContext(), R.color.colorGrayDark)
                        )
                    }
                }
            },
            {_, item ->
                if (item.seat != 0 && item.available) {
                    if (!checkIsAlreadyBook(item.number)) {
                        selected_seat.add(item)
                        selectedSeat.add(item.number)
                        this.checkIsValid()
                        if (selectedSeat.size == selected_order_bus.countOfChair)
                            (rootActivity as ChooseTicketBusActivity).setSelectedSeat(selected_seat)
                        listChooseSeatAdapter.refresh()
                    } else {
                        selectedSeat.remove(item.number)
                        selected_seat.remove(item)
                        this.checkIsValid()
                        listChooseSeatAdapter.refresh()
                    }
                }
            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_seat, container, false)
    }

    private fun checkIsAlreadyBook(number: Int): Boolean{
        selectedSeat.forEach {
            if (it == number) return true
        }
        return false
    }

    private fun checkIsValid() {
        if (selectedSeat.size == selected_order_bus.countOfChair) {
            btn_submit.setTextColor(
                ContextCompat
                    .getColor(requireContext(), R.color.colorWhite)
            )
            btn_submit.setBackgroundResource(R.drawable.background_circle_orange)
            btn_submit.isEnabled = true
        } else {
            btn_submit.isEnabled = false
            btn_submit.setTextColor(
                ContextCompat
                    .getColor(requireContext(), R.color.colorTextLight)
            )
            btn_submit.setBackgroundResource(R.drawable.background_cirlce_dark_gray)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Setup List Choose Seat
        list_choose_seat.layoutManager = GridLayoutManager(requireContext(), 4)
        list_choose_seat.adapter = listChooseSeatAdapter

        // Before
        if (selected_seat.size > 0) {
            selected_seat.forEach {
                selectedSeat.add(it.number)
            }
            this.checkIsValid()
        }

        btn_submit.setOnClickListener {
            requireContext().startActivity(Intent(requireContext(), ComfirmationOrderBusActivity::class.java))
            requireActivity().finish()
        }

        // Load Seat
        this.loadSeat()
    }

    private fun loadSeat() {
        loader.visibility = View.VISIBLE
        list_choose_seat.visibility = View.GONE
        btn_submit.visibility = View.GONE
        val json = JsonObject()
        json.addProperty("id_schedule", scheduleID)
        getSeats(service!!, json, {
            if (it.code == 200) {
                if (it.data.status) {
                    list_choose_seat.layoutManager = GridLayoutManager(requireContext(), it.data.data.layout.seat_column)
                    if (it.data.data.seat.size > 0) listChooseSeatAdapter.data = it.data.data.seat
                } else Toast.makeText(requireContext(), it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_choose_seat.visibility = View.VISIBLE
            btn_submit.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_choose_seat.visibility = View.VISIBLE
            btn_submit.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, scheduleID: String) =
            ChooseSeatFragment().apply {
                this.rootActivity = activity
                this.scheduleID = scheduleID
            }
    }
}