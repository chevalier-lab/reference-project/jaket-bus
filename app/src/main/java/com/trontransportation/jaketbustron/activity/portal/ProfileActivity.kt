package com.trontransportation.jaketbustron.activity.portal

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.CheckAlreadyAuthActivity
import com.trontransportation.jaketbustron.activity.WebActivity
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.apis.ApiClient
import com.trontransportation.jaketbustron.apis.ApiInterface
import com.trontransportation.jaketbustron.models.*
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.component_profile.*
import kotlinx.android.synthetic.main.dialog_profile_edit.view.*
import kotlinx.android.synthetic.main.item_profile.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfileActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil
    private var profileUser: GetBalance.Data? = null
    private lateinit var dialogOrder: AlertDialog
    private var statusLoadData = false
    private lateinit var service: ApiInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        // Setup Toolbar
        this.setupToolbar()

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@ProfileActivity, getString(R.string.path_name))

        val checkAuth = this.cacheUtil.get(auth)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(this@ProfileActivity, SignInActivity::class.java))
            finish()
        }

        this.service = ApiClient.createService(getAuth(this.cacheUtil).token)

        val profileAdapter = AdapterUtil(R.layout.item_profile, listOf(
            ItemProfile(R.drawable.ic_contact, "Kontak"),
            ItemProfile(R.drawable.ic_privation, "Kebijakan Privasi"),
            ItemProfile(R.drawable.ic_role, "Syarat dan Ketentuan"),
            ItemProfile(R.drawable.ic_star, "Beri Kami Nilai")
        ), { itemView, item ->
            itemView.item_profile_name.text = item.label
            itemView.item_profile_icon.setImageResource(item.icon)
        }, { position , item ->
            val intent = Intent(baseContext, WebActivity::class.java)
            intent.putExtra("name", item.label)
            when (position) {
                0 -> intent.putExtra("url", "https://dayangsumbi.trontransportation.id/")
                1 -> intent.putExtra("url", "https://dayangsumbi.trontransportation.id/")
                else -> intent.putExtra("url", "https://dayangsumbi.trontransportation.id/")
            }
            startActivity(intent)
        })
        list_profile.layoutManager = LinearLayoutManager(this@ProfileActivity)
        list_profile.adapter = profileAdapter

        tv_name.text = "-"
        tv_desc.text = "-"
        Glide.with(this@ProfileActivity)
            .load(R.drawable.ic_profile)
            .circleCrop()
            .into(img_profile)

        container_button.setOnClickListener {
            this.editProfileDialog()
        }
        img_profile.setOnClickListener {

        }
        btn_logout.setOnClickListener { logout() }

        // Load profile
        this.loadProfile()
    }

    private fun logout() {
        this.cacheUtil.clear()
        startActivity(Intent(this@ProfileActivity, CheckAlreadyAuthActivity::class.java))
        finish()
    }

    private fun loadProfile() {
        statusLoadData = true

        loader.visibility = View.VISIBLE
        container_profile.visibility = View.GONE
        btn_logout.visibility = View.GONE

        balance(service, {
            if (it.code == 200) {
                // Setup Top Bar
                tv_name.text = StringBuilder().append(it.data.first_name).append(" ").append(it.data.last_name)
                Glide.with(this@ProfileActivity)
                    .load(it.data.utility.face.uri)
                    .circleCrop()
                    .into(img_profile)

                // Setup Wallet Bar
                tv_desc.text = StringBuilder().append(it.data.phone_number).append("\n")
                    .append(it.data.email)

                profileUser = it.data

            } else Toast.makeText(this@ProfileActivity, it.message, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE
            statusLoadData = false

        }, {
            Toast.makeText(this@ProfileActivity, it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE
            statusLoadData = false
        })
    }

    private fun doEditProfile(frontName: String, backName: String) {
        loader.visibility = View.VISIBLE
        container_profile.visibility = View.GONE
        btn_logout.visibility = View.GONE

        val data = JsonObject()
        data.addProperty("first_name", frontName)
        data.addProperty("last_name", backName)
        data.addProperty("level", profileUser!!.utility.level)

        statusLoadData = true

        editProfile(service, data, {

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE

            if (it.code == 200) {
                loadProfile()
            } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE
        })
    }

    private fun editProfileDialog() {
        profileUser?.let {
            // Setup Dialog Logout
            val builder = AlertDialog.Builder(this@ProfileActivity)
            val dialogInflater = layoutInflater
            val dialogView = dialogInflater.inflate(R.layout.dialog_profile_edit, null)

            dialogView.setOnClickListener {

            }

            dialogView.dpe_front_name.setText(it.first_name)
            dialogView.dpe_back_name.setText(it.last_name)

            dialogView.dpe_btn_save.setOnClickListener {
                dialogOrder.dismiss()
                doEditProfile(dialogView.dpe_front_name.text.toString(),
                dialogView.dpe_back_name.text.toString())
            }

            builder.setView(dialogView)
            dialogOrder = builder.create()
            dialogOrder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialogOrder.show()
        }
    }

    private fun doEditPhotoProfile(file: File?) {
        statusLoadData = true
        file?.apply {
            loader.visibility = View.VISIBLE
            container_profile.visibility = View.GONE
            btn_logout.visibility = View.GONE

            val requestBody = RequestBody.create("multipart".toMediaTypeOrNull(), this)
            val data = MultipartBody.Part.createFormData("face",this.name,requestBody)

            editPhotoProfile(service, data, {

                loader.visibility = View.GONE
                container_profile.visibility = View.VISIBLE
                btn_logout.visibility = View.VISIBLE

                if (it.code == 200) {
                     loadProfile()
                } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
            }, {
                Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()

                loader.visibility = View.GONE
                container_profile.visibility = View.VISIBLE
                btn_logout.visibility = View.VISIBLE
            })
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Profile"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!statusLoadData) {
            startActivity(Intent(this@ProfileActivity, HomeActivity::class.java))
            finish()
        } else {
            Toast.makeText(applicationContext, "Harap menunggu, kami sedang mempersiapkan data", Toast.LENGTH_LONG).show()
        }
    }
}