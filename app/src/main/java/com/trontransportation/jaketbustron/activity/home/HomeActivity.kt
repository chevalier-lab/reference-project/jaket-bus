package com.trontransportation.jaketbustron.activity.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.fragments.*
import com.trontransportation.jaketbustron.activity.portal.SignInActivity
import com.trontransportation.jaketbustron.activity.wallets.TopUpActivity
import com.trontransportation.jaketbustron.activity.wallets.WithdrawActivity
import com.trontransportation.jaketbustron.models.GetBalance
import com.trontransportation.jaketbustron.models.balance
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.component_bottom_bar.*
import kotlinx.android.synthetic.main.component_top_bar.*
import kotlinx.android.synthetic.main.component_wallet_bar.*

class HomeActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil
    private lateinit var animationUp: Animation
    private lateinit var animationDown: Animation
    private var isLoadedData = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Setup Animation
        this.animationUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
        this.animationDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@HomeActivity, getString(R.string.path_name))

        val checkAuth = this.cacheUtil.get(auth)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(this@HomeActivity, SignInActivity::class.java))
            finish()
        }

        // Setup Top Bar
        tv_front_name.text = "-"
        Glide.with(this@HomeActivity)
            .load(R.drawable.ic_profile)
            .circleCrop()
            .into(img_profile)

        // Setup Wallet Bar
        tv_balance.text = "-"

        // Handle Top Up
        container_top_up.setOnClickListener {
            startActivity(Intent(this@HomeActivity, TopUpActivity::class.java))
        }

        // Handle Withdraw
        container_payment.setOnClickListener {
            startActivity(Intent(this@HomeActivity, WithdrawActivity::class.java))
        }

        resumeFragment()
    }

    private fun getProfile() {

        balance(service!!, {
            if (it.code == 200) {
                // Setup Top Bar
                tv_front_name.text = it.data.first_name
                Glide.with(this@HomeActivity)
                    .load(it.data.utility.face.uri)
                    .circleCrop()
                    .into(img_profile)

                // Setup Wallet Bar
                tv_balance.text = currencyFormat(it.data.wallet.result.balance)
            } else Toast.makeText(this@HomeActivity, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(this@HomeActivity, it, Toast.LENGTH_LONG).show()
        })
    }

    fun updateProfileGlobal(profile: GetBalance) {
        if (profile.code == 200) {
            // Setup Top Bar
            tv_front_name.text = profile.data.first_name
            Glide.with(this@HomeActivity)
                .load(profile.data.utility.face.uri)
                .circleCrop()
                .into(img_profile)

            // Setup Wallet Bar
            tv_balance.text = currencyFormat(profile.data.wallet.result.balance)
        } else Toast.makeText(this@HomeActivity, profile.message, Toast.LENGTH_LONG).show()
    }

    fun setLoadedDataStatus(status: Boolean) {
        isLoadedData = status
    }

    private fun resumeFragment() {
        bottom_bar.setOnNavigationItemSelectedListener(this.navigationItemSelectedListener)
        bottom_bar.selectedItemId = R.id.item_home
    }

    override fun onResume() {
        super.onResume()

        // Get Profile
        this.getProfile()
    }

    private fun setFragment(fragment: Fragment  ) {
        supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commit()
    }

    private val navigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        if (isLoadedData) {
            return@OnNavigationItemSelectedListener false
        }

        when (it.itemId) {
            R.id.item_home -> {
                container_wallet.visibility = View.VISIBLE
                setLoadedDataStatus(true)
                this.setFragment(HomeFragment.newInstance(this@HomeActivity))
            }
            R.id.item_ticket -> {
                container_wallet.visibility = View.GONE
                setLoadedDataStatus(true)
                this.setFragment(TicketFragment.newInstance(this@HomeActivity))
            }
            R.id.item_bill -> {
                container_wallet.visibility = View.GONE
                setLoadedDataStatus(true)
                this.setFragment(BillFragment.newInstance(this@HomeActivity))
            }
            R.id.item_notification -> {
                container_wallet.visibility = View.GONE
                setLoadedDataStatus(true)
                this.setFragment(NotificationFragment.newInstance(this@HomeActivity))
            }
            R.id.item_profile -> {
                container_wallet.visibility = View.GONE
                setLoadedDataStatus(true)
                this.setFragment(ProfileFragment.newInstance(this@HomeActivity))
            }
        }
        return@OnNavigationItemSelectedListener true
    }
}