package com.trontransportation.jaketbustron.activity.orders.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.orders.OrderBusActivity
import com.trontransportation.jaketbustron.models.ItemLoadRoute
import com.trontransportation.jaketbustron.utilities.AdapterUtil
import com.trontransportation.jaketbustron.utilities.action_load_route_from
import kotlinx.android.synthetic.main.fragment_load_route.*
import kotlinx.android.synthetic.main.item_load_route.view.*
import java.util.*

class LoadRouteFragment : Fragment() {

    internal lateinit var rootLayout: Activity
    internal lateinit var action: String
    private lateinit var loadRouteAdapter: AdapterUtil<ItemLoadRoute>
    private var data: MutableList<ItemLoadRoute> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Load Adapter
        loadRouteAdapter = AdapterUtil(R.layout.item_load_route, listOf(),
            {itemView, item ->
                if (action == action_load_route_from)
                    itemView.item_load_route_type.text = StringBuilder().append("Keberangkatan")
                else
                    itemView.item_load_route_type.text = StringBuilder().append("Tujuan")
                itemView.item_load_route_province.text = item.city
                itemView.item_load_route_city.text = item.province
            },
            {_, item ->
                (rootLayout as OrderBusActivity).setSelectedRoute(action, item)
            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_load_route, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup List load Route
        list_load_route.layoutManager = LinearLayoutManager(requireContext())
        list_load_route.adapter = loadRouteAdapter

        // Load Route
//        this.loadRoute()

        // Live Search
        filter_route.addTextChangedListener {
            if (it?.length == 0) loadRouteAdapter.data = data
            else {
                val tempData: MutableList<ItemLoadRoute> = arrayListOf()
                val currentSearch = filter_route.text.toString().toLowerCase(Locale.getDefault())
                data.forEach {item ->
                    if (item.city.contains(currentSearch, ignoreCase = true)) tempData.add(item)
                    else if (item.province.contains(currentSearch, ignoreCase = true)) tempData.add(item)
                }
                loadRouteAdapter.data = tempData
            }
        }

        loadRouteAdapter.data = this.data
    }

    companion object {
        fun newInstance(activity: Activity, action: String, route: MutableList<ItemLoadRoute>) =
            LoadRouteFragment().apply {
                this.action = action
                this.rootLayout = activity
                this.data = route
            }
    }
}