package com.trontransportation.jaketbustron.activity.portal

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.utilities.create_user
import com.trontransportation.jaketbustron.utilities.phone_number_otp
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.btn_sign_up
import kotlinx.android.synthetic.main.activity_sign_up.tv_phone_number

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // Setup Toolbar
        this.setupToolbar()

        // Render HTML Code
        tv_sign_in.text = Html.fromHtml(getString(R.string.message_have_account))
        msg_policy.text = Html.fromHtml(getString(R.string.message_registration_policy))

        // Goto Sign In
        tv_sign_in.setOnClickListener { onBackPressed() }

        // Handler Phone Number
        btn_sign_up.isEnabled = false
        tv_phone_number.setOnClickListener {
            tv_phone_number.isFocusableInTouchMode = true
            tv_phone_number.isFocusable = true
        }
        tv_phone_number.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                p0?.apply {
                    if (this.isNotEmpty() && this[0] == '0') {
                        this.replace(0, 1, "")
                    }
                }

                if (p0!!.length >= 8) {
                    btn_sign_up.isEnabled = true
                    btn_sign_up.setBackgroundResource(R.drawable.background_circle_orange)
                }
            }
        })

        // Goto Sign Up Profile
        btn_sign_up.setOnClickListener {
            if (TextUtils.isEmpty(tv_phone_number.text)) {
                Toast.makeText(this@SignUpActivity,
                "Harap isi nomor telepon", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            create_user.phone_number = "62${tv_phone_number.text}"
            phone_number_otp = "+62${tv_phone_number.text}"

            startActivity(Intent(this@SignUpActivity, SignUpProfileActivity::class.java))
            finish()
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Registrasi JAKET BUS"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@SignUpActivity, SignInActivity::class.java))
        finish()
    }
}