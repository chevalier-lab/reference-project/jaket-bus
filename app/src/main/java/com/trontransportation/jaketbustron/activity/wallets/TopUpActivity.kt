package com.trontransportation.jaketbustron.activity.wallets

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.WebActivity
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.models.DepositHistory
import com.trontransportation.jaketbustron.models.topUp
import com.trontransportation.jaketbustron.models.topUpList
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_top_up.*
import kotlinx.android.synthetic.main.item_withdraw.view.*
import java.lang.StringBuilder

class TopUpActivity : AppCompatActivity() {

    private var statusLoadData = false
    private lateinit var topUpAdapter: AdapterUtil<DepositHistory.Data.Result.ItemDepositHistory>

    private var paymentType = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up)

        // Setup Toolbar
        this.setupToolbar()

        edt_type.text = StringBuilder().append("Kaspro")
        edt_type.setOnClickListener {
            val builder = AlertDialog.Builder(this@TopUpActivity)
            val type = listOf("Kaspro", "Transfer Bank", "Gopay").toTypedArray()
            builder.setTitle("Jenis Payment")
                .setItems(
                    type
                ) { dialog, which ->
                    paymentType = when (which) {
                        0 -> 5
                        1 -> 6
                        else -> 7
                    }
                    edt_type.text = type[which]
                    dialog.dismiss()
                }
            builder.create().show()
        }

        btn_top_up.setOnClickListener {
            if (TextUtils.isEmpty(edt_amount.text.toString())) {
                Toast.makeText(applicationContext, "Harap isi jumlah top up", Toast.LENGTH_LONG).show()
            } else doTopUp()
        }

        topUpAdapter = AdapterUtil(R.layout.item_withdraw, listOf(),
            {itemView, item ->
                itemView.item_withdraw_name.text = convertStringToIDFormat(item.createdAt)
                itemView.item_withdraw_status.text = item.status

                when (item.status) {
                    "pending" -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_orange)
                    "success" -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_green)
                    else -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_red)
                }

                itemView.item_withdraw_amount.text = currencyFormat(item.amount)
            },
            {_, _ -> })

        list_top_up_history.layoutManager = LinearLayoutManager(this@TopUpActivity)
        list_top_up_history.adapter = topUpAdapter
    }

    override fun onResume() {
        this.loadTopUpHistory()
        super.onResume()
    }

    private fun loadTopUpHistory() {
        loader.visibility = View.VISIBLE
        heading_top_up.visibility = View.GONE
        list_top_up_history.visibility = View.GONE
        topUpList(service!!, 0, {

            loader.visibility = View.GONE
            heading_top_up.visibility = View.VISIBLE
            list_top_up_history.visibility = View.VISIBLE

            if (it.code == 200) {
                if (it.data.success) {
                    topUpAdapter.data = it.data.result.data
                } else Toast.makeText(applicationContext, it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
        }, {
            loader.visibility = View.GONE
            heading_top_up.visibility = View.VISIBLE
            list_top_up_history.visibility = View.VISIBLE

            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun doTopUp() {
        loader_btn_top_up.visibility = View.VISIBLE
        btn_top_up.visibility = View.GONE
        val data = JsonObject()
        data.addProperty("amount", edt_amount.text.toString())
        data.addProperty("payment_id", paymentType)
        topUp(service!!, data, {
            loader_btn_top_up.visibility = View.GONE
            btn_top_up.visibility = View.VISIBLE
            if (it.code == 200) {
                if (it.data.success) {
                    edt_amount.setText("")
                    if (paymentType == 5) {
                        Toast.makeText(
                            applicationContext,
                            "Berhasil melakukan top up",
                            Toast.LENGTH_LONG
                        ).show()
                        loadTopUpHistory()
                    } else {
                        val intent = Intent(this@TopUpActivity, WebActivity::class.java)
                        intent.putExtra("name", "Pembayaran")
                        intent.putExtra("url", it.data.result!!.redirect_url)
                        startActivity(intent)
                    }
                } else Toast.makeText(applicationContext, it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
        }, {
            loader_btn_top_up.visibility = View.GONE
            btn_top_up.visibility = View.VISIBLE
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Top Up"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!statusLoadData) {
            startActivity(Intent(this@TopUpActivity, HomeActivity::class.java))
            finish()
        } else {
            Toast.makeText(applicationContext, "Harap menunggu, kami sedang mempersiapkan data", Toast.LENGTH_LONG).show()
        }
    }
}