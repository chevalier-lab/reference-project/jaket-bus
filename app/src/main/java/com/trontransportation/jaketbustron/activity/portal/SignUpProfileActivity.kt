package com.trontransportation.jaketbustron.activity.portal

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.models.signUp
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_sign_up_profile.*

class SignUpProfileActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_profile)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@SignUpProfileActivity, getString(R.string.path_name))

        // Setup Toolbar
        this.setupToolbar()

        // Render HTML Code
        msg_policy.text = Html.fromHtml(getString(R.string.message_registration_policy))

        // Handler Inputs
        tv_front_name.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                if (checkIsFilled()) {
                    btn_sign_up.isEnabled = true
                    btn_sign_up.setBackgroundResource(R.drawable.background_circle_orange)
                }
            }
        })
        tv_back_name.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                if (checkIsFilled()) {
                    btn_sign_up.isEnabled = true
                    btn_sign_up.setBackgroundResource(R.drawable.background_circle_orange)
                }
            }
        })
        tv_email.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                if (checkIsFilled()) {
                    btn_sign_up.isEnabled = true
                    btn_sign_up.setBackgroundResource(R.drawable.background_circle_orange)
                }
            }
        })
        tv_password.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                if (checkIsFilled()) {
                    btn_sign_up.isEnabled = true
                    btn_sign_up.setBackgroundResource(R.drawable.background_circle_orange)
                }
            }
        })

        // Show Hide Password
        var isShow = false
        tv_password.setOnTouchListener { view, event ->
            val DRAWABLE_RIGHT = 2;

            if(event.action == MotionEvent.ACTION_UP) {
                if(event.rawX >=
                    (tv_password.right - tv_password.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    if (isShow) {
                        isShow = false
                        tv_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_close, 0)
                        tv_password.transformationMethod = PasswordTransformationMethod.getInstance()
                    } else {
                        isShow = true
                        tv_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_open, 0)
                        tv_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }

                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }

        // Goto OTP
        btn_sign_up.setOnClickListener {
            // Validate Inputs
            when {
                TextUtils.isEmpty(tv_front_name.text) -> {
                    Toast.makeText(this@SignUpProfileActivity,
                    "Masukkan nama depan", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                TextUtils.isEmpty(tv_back_name.text) -> {
                    Toast.makeText(this@SignUpProfileActivity,
                        "Masukkan nama belakang", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                TextUtils.isEmpty(tv_email.text) -> {
                    Toast.makeText(this@SignUpProfileActivity,
                        "Masukkan email", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                TextUtils.isEmpty(tv_password.text) -> {
                    Toast.makeText(this@SignUpProfileActivity,
                        "Masukkan password", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                !Patterns.EMAIL_ADDRESS.matcher(tv_email.text).matches() -> {
                    Toast.makeText(this@SignUpProfileActivity,
                        "Format e-mail salah", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
            }

            // Insert to temp
            create_user.email = tv_email.text.toString()
            create_user.first_name = tv_front_name.text.toString()
            create_user.last_name = tv_back_name.text.toString()
            create_user.password = tv_password.text.toString()
            create_user.level = 1

            // Create User
            this.createUser()
        }
    }

    private fun createUser() {
        val json = JsonObject()
        json.addProperty("first_name", create_user.first_name)
        json.addProperty("last_name", create_user.last_name)
        json.addProperty("email", create_user.email)
        json.addProperty("password", create_user.password)
        json.addProperty("phone_number", create_user.phone_number)
        json.addProperty("level", create_user.level)

        container_form.visibility = View.GONE
        loader.visibility = View.VISIBLE

        signUp(json, {
            if (it.code == 200) {
                this.cacheUtil.set(authBefore, it.data)

                // Create user
                before_otp = 1
                startActivity(Intent(this@SignUpProfileActivity, OTPActivity::class.java))
                finish()
            } else Toast.makeText(this@SignUpProfileActivity, it.message, Toast.LENGTH_LONG).show()

            container_form.visibility = View.VISIBLE
            loader.visibility = View.GONE
        }, {
            Toast.makeText(this@SignUpProfileActivity, it, Toast.LENGTH_LONG).show()
            container_form.visibility = View.VISIBLE
            loader.visibility = View.GONE
        })
    }

    private fun checkIsFilled(): Boolean {
        return !(TextUtils.isEmpty(tv_front_name.text.toString()) ||
                TextUtils.isEmpty(tv_back_name.text.toString()) ||
                TextUtils.isEmpty(tv_email.text.toString()) ||
                TextUtils.isEmpty(tv_password.text.toString()))
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Registrasi JAKET BUS"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@SignUpProfileActivity, SignUpActivity::class.java))
        finish()
    }
}