package com.trontransportation.jaketbustron.activity.schedule.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.models.ItemChooseTicket
import com.trontransportation.jaketbustron.utilities.AdapterUtil
import kotlinx.android.synthetic.main.fragment_facility.*
import kotlinx.android.synthetic.main.item_facility.view.*

class FacilityFragment : Fragment() {

    private lateinit var selectedTicket: ItemChooseTicket
    private lateinit var listFacilityAdapter: AdapterUtil<ItemChooseTicket.Facility>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Set Adapter
        listFacilityAdapter = AdapterUtil(R.layout.item_facility, selectedTicket.facility,
            {itemView, item ->
                itemView.item_facility_label.text = item.facility_name
                Glide.with(itemView)
                    .load(item.facility_icon)
                    .error(R.drawable.background_cirlce_dark_gray)
                    .into(itemView.item_facility_icon)
            }, {_, _ ->

            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_facility, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup List Facility
        list_facility.layoutManager = LinearLayoutManager(requireContext())
        list_facility.adapter = listFacilityAdapter
    }

    companion object {
        @JvmStatic
        fun newInstance(ticket: ItemChooseTicket) =
            FacilityFragment().apply {
                this.selectedTicket = ticket
            }
    }
}