package com.trontransportation.jaketbustron.activity.portal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.models.signIn
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@SignInActivity, getString(R.string.path_name))

        // Render HTML Code
        msg_policy.text = Html.fromHtml(getString(R.string.message_registration_policy))

        // Set Request Focusable
        tv_phone_number.setOnClickListener {
            tv_phone_number.isFocusableInTouchMode = true
            tv_phone_number.isFocusable = true
        }

        // Goto Otp
        btn_sign_in.setOnClickListener {

            if (TextUtils.isEmpty(tv_phone_number.text.toString())) {
                Toast.makeText(this@SignInActivity,
                "Harap isi nomor telepon", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val json = JsonObject()
            json.addProperty("phone_number", "62${tv_phone_number.text}")

            phone_number_otp = "+62${tv_phone_number.text}"

            loader.visibility = View.VISIBLE
            container_login.visibility = View.GONE
            container_registration.visibility = View.GONE
            signIn(json, {
                loader.visibility = View.GONE
                container_login.visibility = View.VISIBLE
                container_registration.visibility = View.VISIBLE
                if (it.code == 200) {
                    this.cacheUtil.set(authBefore, it.data)
                    before_otp = 0
                    startActivity(Intent(this@SignInActivity, OTPActivity::class.java))
                    finish()
                } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
            }, {
                loader.visibility = View.GONE
                container_login.visibility = View.VISIBLE
                container_registration.visibility = View.VISIBLE
                Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
            })
        }

        // Goto Sign Up
        btn_sign_up.setOnClickListener {
            startActivity(Intent(this@SignInActivity, SignUpActivity::class.java))
            finish()
        }
    }
}