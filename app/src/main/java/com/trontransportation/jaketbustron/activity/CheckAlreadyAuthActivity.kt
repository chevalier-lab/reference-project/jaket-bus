package com.trontransportation.jaketbustron.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.portal.SignInActivity
import com.trontransportation.jaketbustron.utilities.CacheUtil
import com.trontransportation.jaketbustron.utilities.auth
import com.trontransportation.jaketbustron.utilities.createService
import com.trontransportation.jaketbustron.utilities.service

class CheckAlreadyAuthActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_already_auth)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@CheckAlreadyAuthActivity, getString(R.string.path_name))

        if (TextUtils.isEmpty(this.cacheUtil.get(auth)))
            startActivity(Intent(this@CheckAlreadyAuthActivity, SignInActivity::class.java))
        else {
            createService(this.cacheUtil)
            startActivity(Intent(this@CheckAlreadyAuthActivity, HomeActivity::class.java))
        }
        finish()
    }
}