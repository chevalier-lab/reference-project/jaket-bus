package com.trontransportation.jaketbustron.activity.home.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.orders.TicketInfoActivity
import com.trontransportation.jaketbustron.apis.ApiClient
import com.trontransportation.jaketbustron.apis.ApiInterface
import com.trontransportation.jaketbustron.models.GetBookingTicket
import com.trontransportation.jaketbustron.models.getTicket
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.fragment_ticket.*
import kotlinx.android.synthetic.main.item_ticket.view.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class TicketFragment : Fragment() {

    private lateinit var listTicketAdapter: AdapterUtil<GetBookingTicket.Data>
    private lateinit var rootActivity: HomeActivity
    private var isLoadedTicket = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_ticket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rootActivity.setLoadedDataStatus(true)

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        // Setup List Ticket
        listTicketAdapter = AdapterUtil(R.layout.item_ticket, listOf(),
            { itemView, item ->
                itemView.item_ticket_tv_id_ticket.text = StringBuilder()
                    .append("ID-")
                    .append(item.ticket_id)
                itemView.item_ticket_tv_po_name.text = item.po_name
                itemView.item_ticket_tv_route_from.text = item.origin
                itemView.item_ticket_tv_time_go.text = item.time_of_departure
                itemView.item_ticket_tv_route_to.text = item.destionation
                itemView.item_ticket_tv_price.text = numberFormat.format(item.price)
                itemView.item_ticket_tv_type.text = item.category
                itemView.item_ticket_tv_passenger_name.text = item.passanger_name
                itemView.item_ticket_tv_seat_available.text = StringBuilder()
                    .append("Kursi-").append(item.seat_number)
                itemView.item_ticket_tv_date.text = convertDateToIDFormat(
                    SimpleDateFormat("yyyy-MM-dd",
                        Locale.getDefault()).parse(item.date_of_departure)!!)
            }, {_, item ->
                selected_ticket = item
                startActivity(Intent(requireContext(), TicketInfoActivity::class.java))
            })

        // Setup List Ticket
        list_ticket.layoutManager = LinearLayoutManager(requireContext())
        list_ticket.adapter = listTicketAdapter
        list_ticket.isNestedScrollingEnabled = false

        // Load My Ticket
        this.loadMyTicket()
    }

    override fun onResume() {
        super.onResume()
        this.loadMyTicket()
    }

    private fun checkIsAlreadyLoaded() {
        if (!isLoadedTicket) rootActivity.setLoadedDataStatus(false)
    }

    private fun loadMyTicket() {
        loader.visibility = View.VISIBLE
        list_ticket.visibility = View.GONE

        isLoadedTicket = true

        val json = JsonObject()
        json.addProperty("page", 1)
        getTicket(service!!, json, {
            if (it.code == 200) {
                listTicketAdapter.data = it.data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_ticket.visibility = View.VISIBLE

            isLoadedTicket = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_ticket.visibility = View.VISIBLE

            isLoadedTicket = false
            checkIsAlreadyLoaded()
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(rootActivity: HomeActivity) = TicketFragment().apply {
            this.rootActivity = rootActivity
        }
    }
}