package com.trontransportation.jaketbustron.activity.schedule

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.schedule.fragments.ChooseSeatFragment
import com.trontransportation.jaketbustron.activity.schedule.fragments.FacilityFragment
import com.trontransportation.jaketbustron.models.ItemChooseTicket
import com.trontransportation.jaketbustron.models.getSchedule
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_choose_schedule.*
import kotlinx.android.synthetic.main.component_sheet_with_tab_layout.*
import kotlinx.android.synthetic.main.item_choose_ticket_bus.view.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class ChooseScheduleActivity : AppCompatActivity() {

    private lateinit var listChooseTicketAdapter: AdapterUtil<ItemChooseTicket>
    private lateinit var animationUp: Animation
    private lateinit var animationDown: Animation
    private var isShowSheet = false
    private var selectedSchedule: ItemChooseTicket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_schedule)

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        // Setup Toolbar
        this.setupToolbar()

        // Setup Animation
        this.animationUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
        this.animationDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)

        // Set Tab Sheet
        sheet_tab.addTab(sheet_tab.newTab().setText("Daftar Kursi"))
        sheet_tab.addTab(sheet_tab.newTab().setText("Fasilitas"))

        sheet_tab.setSelectedTabIndicatorColor(
            ContextCompat.getColor(this@ChooseScheduleActivity, R.color.colorPrimary))
        sheet_tab.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val fragment: Fragment = when (tab!!.position) {
                    0 -> ChooseSeatFragment.newInstance(
                        selectedSchedule!!.id_schedule.toString())
                    else -> FacilityFragment.newInstance(selectedSchedule!!)
                }
                supportFragmentManager.beginTransaction().replace(R.id.sheet_content, fragment)
                    .commit()
            }
        })

        // Setup List Choose Ticket
        listChooseTicketAdapter = AdapterUtil(R.layout.item_choose_ticket_bus, listOf(),
            {itemView, item ->
                itemView.item_choose_tv_po_name.text = item.po_name
                itemView.item_choose_tv_route_from_code.text = selected_order_bus.routeFrom!!.city
                itemView.item_choose_tv_route_from.text = selected_order_bus.routeFrom!!.province
                itemView.item_choose_tv_time_go.text = item.time_of_departure
                itemView.item_choose_tv_route_to_code.text = selected_order_bus.routeTo!!.city
                itemView.item_choose_tv_route_to.text = selected_order_bus.routeTo!!.province
                itemView.item_choose_tv_price.text = numberFormat.format(item.price)
                itemView.item_choose_tv_seat_available.text = StringBuilder()
                    .append("Sisa kursi ").append(item.available)
                itemView.item_choose_tv_type.text = item.type_of_bus
                itemView.item_choose_tv_date.text = item.date_of_departure

                itemView.item_choose_btn_choose_ticket.setOnClickListener {
                    selectedSchedule = item
                    supportFragmentManager.beginTransaction().replace(R.id.sheet_content,
                        ChooseSeatFragment.newInstance(
                            item.id_schedule.toString()))
                        .commit()
                    isShowSheet = true
                    sheet_choose_ticket.visibility = View.VISIBLE
                    sheet_choose_ticket.startAnimation(animationUp)
                }
            },
            {_, item ->
                selectedSchedule = item
                supportFragmentManager.beginTransaction().replace(R.id.sheet_content,
                    ChooseSeatFragment.newInstance(
                        item.id_schedule.toString()))
                    .commit()
                isShowSheet = true
                sheet_choose_ticket.visibility = View.VISIBLE
                sheet_choose_ticket.startAnimation(animationUp)
            })
        list_choose_ticket_bus.layoutManager = LinearLayoutManager(this@ChooseScheduleActivity)
        list_choose_ticket_bus.adapter = listChooseTicketAdapter

        this.loadTicket()
    }

    private fun loadTicket() {
        loader.visibility = View.VISIBLE
        list_choose_ticket_bus.visibility = View.GONE

        val date = SimpleDateFormat("yyyy-MM-dd", Locale("ID"))

        val json = JsonObject()
        json.addProperty("destionation_id", selected_order_bus.routeTo!!.id)
        json.addProperty("id_of_origin", selected_order_bus.routeFrom!!.id)
        json.addProperty("date_of_departure", date.format(selected_order_bus.scheduleGo!!))

        getSchedule(service!!, json, {
            if (it.code == 200) {
                if (it.data.status) {

                    val data: MutableList<ItemChooseTicket> = arrayListOf()
                    it.data.data.forEach {item ->
                        val facility: MutableList<ItemChooseTicket.Facility> = arrayListOf()
                        item.facility.forEach {itemFacility ->
                            facility.add(ItemChooseTicket.Facility(
                                itemFacility.facility_name,
                                itemFacility.facility_icon
                            ))
                        }
                        data.add(ItemChooseTicket(
                            item.id_schedule,
                            item.id_info,
                            item.date_of_departure,
                            item.time_of_departure,
                            item.price,
                            item.id_of_origin,
                            item.destionation_id,
                            item.route,
                            item.id_category,
                            item.id_po,
                            item.total_passenger,
                            item.po_name,
                            item.destionation,
                            item.origin,
                            item.category,
                            item.available,
                            facility
                        ))
                    }
                    if (data.size > 0) listChooseTicketAdapter.data = data

                } else Toast.makeText(this@ChooseScheduleActivity, it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(this@ChooseScheduleActivity, it.message, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            list_choose_ticket_bus.visibility = View.VISIBLE
        }, {
            Toast.makeText(this@ChooseScheduleActivity, it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            list_choose_ticket_bus.visibility = View.VISIBLE
        })
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "${selected_order_bus.routeFrom?.city} - ${selected_order_bus.routeTo?.city}"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isShowSheet) {
            isShowSheet = false
            sheet_choose_ticket.startAnimation(animationDown)
            sheet_choose_ticket.visibility = View.GONE
        } else {
            startActivity(Intent(this@ChooseScheduleActivity, ScheduleInfoActivity::class.java))
            finish()
        }
    }
}