package com.trontransportation.jaketbustron.activity.portal

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.CheckAlreadyAuthActivity
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_o_t_p.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class OTPActivity : AppCompatActivity() {
    private lateinit var timer: CountDownTimer
    private lateinit var verificationKey: String
    private lateinit var progressDialog: ProgressDialog
    private lateinit var cacheUtil: CacheUtil
    companion object {
        val TAG = "OTP"
    }

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_o_t_p)

        this.firebaseAuth = FirebaseAuth.getInstance()
        // Setup Toolbar
        this.setupToolbar()

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@OTPActivity, getString(R.string.path_name))

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(true)

        // Set Timer
        timer = object: CountDownTimer((180 * 1000), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val df = SimpleDateFormat("mm:ss", Locale.US)
                tv_count_down.text = df.format(Date(millisUntilFinished)).toString()
            }

            override fun onFinish() {
                tv_reload_verification_code.visibility = View.VISIBLE
            }
        }
        timer.start()

        // Set Handler For Focusable Inputs
        tv_code_1.setOnClickListener {
            setFocusable()
        }
        tv_code_2.setOnClickListener {
            setFocusable()
        }
        tv_code_3.setOnClickListener {
            setFocusable()
        }
        tv_code_4.setOnClickListener {
            setFocusable()
        }
        tv_code_5.setOnClickListener {
            setFocusable()
        }
        tv_code_6.setOnClickListener {
            setFocusable()
        }

        // Reload verification code
        tv_reload_verification_code.setOnClickListener {
            tv_reload_verification_code.visibility = View.INVISIBLE
            timer.cancel()
            timer.start()
        }

        tv_code_1.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    return
                }

                if (checkIsFilled()) {
                    tv_code_1.clearFocus()
                    tv_code_2.clearFocus()
                    tv_code_3.clearFocus()
                    tv_code_4.clearFocus()

                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else {
                    tv_code_2.requestFocus()
                }
            }
        })
        tv_code_2.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    tv_code_1.requestFocus()
                    return
                }

                if (checkIsFilled()) {
                    tv_code_1.clearFocus()
                    tv_code_2.clearFocus()
                    tv_code_3.clearFocus()
                    tv_code_4.clearFocus()

                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else tv_code_3.requestFocus()
            }
        })
        tv_code_3.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    tv_code_2.requestFocus()
                    return
                }

                if (checkIsFilled()) {
                    tv_code_1.clearFocus()
                    tv_code_2.clearFocus()
                    tv_code_3.clearFocus()
                    tv_code_4.clearFocus()

                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else tv_code_4.requestFocus()
            }
        })
        tv_code_4.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    tv_code_3.requestFocus()
                    return
                }

                if (checkIsFilled()) {
                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else tv_code_5.requestFocus()
            }
        })
        tv_code_5.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    tv_code_4.requestFocus()
                    return
                }

                if (checkIsFilled()) {
                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else tv_code_6.requestFocus()
            }
        })
        tv_code_6.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {
                    tv_code_5.requestFocus()
                    return
                }

                if (checkIsFilled()) {
                    tv_code_1.clearFocus()
                    tv_code_2.clearFocus()
                    tv_code_3.clearFocus()
                    tv_code_4.clearFocus()
                    tv_code_5.clearFocus()
                    tv_code_6.clearFocus()
                    hideKeyboard()
                    btn_verification.isEnabled = true
                    btn_verification.setBackgroundResource(R.drawable.background_circle_orange)
                } else tv_code_1.requestFocus()
            }
        })

        // Set Message Verification
        msg_verification_info.text = StringBuilder().append("Kode verifikasi telah dikirimkan ke ")
            .append("xxxxxxxx").append(",\n").append("apabila belum menerima kode verifikasi sampai waktu habis, silahkan meminta kode baru")

        // Goto Home
        btn_verification.setOnClickListener {
            val credential = PhoneAuthProvider.getCredential(verificationKey,
            StringBuilder().append(tv_code_1.text)
                .append(tv_code_2.text)
                .append(tv_code_3.text)
                .append(tv_code_4.text)
                .append(tv_code_5.text)
                .append(tv_code_6.text).toString())
            signInWithPhoneAuthCredential(credential)
        }

        // Load OTP
        this.loadOTP()
    }

    fun AppCompatActivity.hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(findViewById<View>(android.R.id.content).windowToken, 0);
    }

    private fun loadOTP() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phone_number_otp, // Phone number to verify
            120, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            callbacks) // OnVerificationStateChangedCallbacks
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        progressDialog.show()
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                progressDialog.dismiss()
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")

                    val user = task.result?.user
                    timer.cancel()

                    val before = getAuthBeofre(cacheUtil)
                    this.cacheUtil.set(auth, before)

                    startActivity(Intent(this@OTPActivity, CheckAlreadyAuthActivity::class.java))
                    finish()
                    // ...
                } else {
                    // Sign in failed, display a message and update the UI
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                    }
                }
            }
    }

    private val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onVerificationCompleted(credential: PhoneAuthCredential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            Log.d(TAG, "onVerificationCompleted:$credential")
        }

        override fun onVerificationFailed(e: FirebaseException) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Log.w(TAG, "onVerificationFailed", e)

            if (e is FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
                Log.d(TAG, "onFailedSendCode:${e.message}")
            } else if (e is FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
                Log.d(TAG, "onFailedSendCode:${e.message}")
            }

            // Show a message and update the UI
            // ...
        }

        override fun onCodeSent(
            verificationId: String,
            token: PhoneAuthProvider.ForceResendingToken
        ) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Log.d(TAG, "onCodeSent:$verificationId")
            Log.d(TAG, "onCodeSentToken:$token")

            verificationKey = verificationId

            // Save verification ID and resending token so we can use them later
//            storedVerificationId = verificationId
//            resendToken =

            // ...
        }
    }

    private fun setFocusable() {
        tv_code_1.isFocusableInTouchMode = true
        tv_code_1.isFocusable = true
        tv_code_2.isFocusableInTouchMode = true
        tv_code_2.isFocusable = true
        tv_code_3.isFocusableInTouchMode = true
        tv_code_3.isFocusable = true
        tv_code_4.isFocusableInTouchMode = true
        tv_code_4.isFocusable = true
        tv_code_5.isFocusableInTouchMode = true
        tv_code_5.isFocusable = true
        tv_code_6.isFocusableInTouchMode = true
        tv_code_6.isFocusable = true
    }

    private fun checkIsFilled(): Boolean {
        return !(TextUtils.isEmpty(tv_code_1.text.toString()) ||
                TextUtils.isEmpty(tv_code_2.text.toString()) ||
                TextUtils.isEmpty(tv_code_3.text.toString()) ||
                TextUtils.isEmpty(tv_code_4.text.toString()) ||
                TextUtils.isEmpty(tv_code_5.text.toString()) ||
                TextUtils.isEmpty(tv_code_6.text.toString()))
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "OTP"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (before_otp == 0)
            startActivity(Intent(this@OTPActivity, SignInActivity::class.java))
        else
            startActivity(Intent(this@OTPActivity, SignUpProfileActivity::class.java))
        finish()
    }
}