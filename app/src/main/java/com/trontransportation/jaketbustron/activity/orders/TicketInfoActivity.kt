package com.trontransportation.jaketbustron.activity.orders

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.fragments.FacilityTicketFragment
import com.trontransportation.jaketbustron.activity.home.fragments.InfoTicketFragment
import com.trontransportation.jaketbustron.utilities.selected_ticket
import kotlinx.android.synthetic.main.activity_ticket_info.*

class TicketInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_info)

        // Setup Toolbar
        this.setupToolbar()

        tab.addTab(tab.newTab().setText("Detail Tiket"))
        tab.addTab(tab.newTab().setText("Fasilitas"))

        supportFragmentManager.beginTransaction().replace(R.id.content,
            InfoTicketFragment.newInstance(this@TicketInfoActivity, selected_ticket!!))
            .commit()

        tab.setSelectedTabIndicatorColor(
            ContextCompat.getColor(this@TicketInfoActivity, R.color.colorPrimary))
        tab.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val fragment: Fragment = when (tab!!.position) {
                    0 -> InfoTicketFragment.newInstance(this@TicketInfoActivity, selected_ticket!!)
                    else -> FacilityTicketFragment.newInstance(selected_ticket!!)
                }
                supportFragmentManager.beginTransaction().replace(R.id.content, fragment)
                    .commit()
            }
        })
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Informasi Tiket"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}