package com.trontransportation.jaketbustron.activity.home.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.models.GetBookingTicket
import com.trontransportation.jaketbustron.utilities.convertStringToIDFormat2
import kotlinx.android.synthetic.main.fragment_info_ticket.*
import kotlinx.android.synthetic.main.fragment_info_ticket.img_qr_code
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_po_name
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_price_total
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_route_from
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_route_to
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_schedule_go
import kotlinx.android.synthetic.main.fragment_info_ticket.tv_type
import java.text.NumberFormat
import java.util.*

class InfoTicketFragment : Fragment() {

    private lateinit var rootActivity: Activity
    private lateinit var selectedTicket: GetBookingTicket.Data

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_ticket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        // Setup Ticket
        tv_po_name.text = this.selectedTicket.po_name
        tv_ticket_id.text = StringBuilder().append("ID-")
            .append(this.selectedTicket.ticket_id)
        tv_route_from.text = this.selectedTicket.origin
        tv_route_to.text = this.selectedTicket.destionation
        tv_schedule_go.text = StringBuilder().append(convertStringToIDFormat2(this.selectedTicket.date_of_departure))
            .append("\n").append(this.selectedTicket.time_of_departure)
//        tv_chair.text = StringBuilder().append(this.selectedTicket.)
        tv_passenger_name.text = this.selectedTicket.passanger_name
        tv_chair.text = StringBuilder().append(this.selectedTicket.seat_number)
        tv_price_total.text = numberFormat.format(this.selectedTicket.price)
        tv_type.text = this.selectedTicket.category

        // Setup QR Code
        val qrPlainText = StringBuilder().append(this.selectedTicket.pnr).toString()
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(qrPlainText, BarcodeFormat.QR_CODE,480,480)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            img_qr_code.setImageBitmap(bitmap)
            tx_qr_code.text = qrPlainText
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, ticket: GetBookingTicket.Data) =
            InfoTicketFragment().apply {
                this.rootActivity = activity
                this.selectedTicket = ticket
            }
    }
}