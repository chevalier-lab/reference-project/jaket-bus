package com.trontransportation.jaketbustron.activity.orders

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.utilities.convertStringToIDFormat2
import com.trontransportation.jaketbustron.utilities.selected_ticket_order
import kotlinx.android.synthetic.main.activity_order_detail.*
import java.text.NumberFormat
import java.util.*

class OrderDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)

        // Setup Toolbar
        this.setupToolbar()

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        tv_po_name.text = selected_ticket_order!!.po_name
        tv_ticket_id.text = StringBuilder().append(selected_ticket_order!!.payment_status)
        tv_route_from.text = selected_ticket_order!!.origin
        tv_route_to.text = selected_ticket_order!!.destionation
        tv_schedule_go.text = StringBuilder().append(convertStringToIDFormat2(selected_ticket_order!!.date_of_departure))
            .append("\n").append(selected_ticket_order!!.time_of_departure)
//        tv_chair.text = StringBuilder().append(this.selectedTicket.)
        tv_passenger_name.text = selected_ticket_order!!.passanger_name
        tv_chair.text = StringBuilder().append(selected_ticket_order!!.seat_number)
        tv_price_total.text = numberFormat.format(selected_ticket_order!!.price)
        tv_type.text = selected_ticket_order!!.category
        tv_payment_method.text = if (selected_ticket_order!!.payment_method == "balance") "Dompet Jak" else "Indomaret"

        // Setup QR Code
        val qrPlainText = StringBuilder().append(selected_ticket_order!!.payment_code).toString()
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(qrPlainText, BarcodeFormat.QR_CODE,480,480)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            img_qr_code.setImageBitmap(bitmap)
            tx_qr_code.text = StringBuilder().append("Kode Pembayaran: ").append(qrPlainText)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Detail Tagihan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}