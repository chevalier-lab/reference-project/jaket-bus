package com.trontransportation.jaketbustron.activity.home.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.BuildConfig
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.CheckAlreadyAuthActivity
import com.trontransportation.jaketbustron.activity.WebActivity
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.portal.SignInActivity
import com.trontransportation.jaketbustron.models.*
import com.trontransportation.jaketbustron.utilities.*
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import id.zelory.compressor.constraint.size
import kotlinx.android.synthetic.main.component_profile.*
import kotlinx.android.synthetic.main.dialog_profile_edit.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.item_profile.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfileFragment : Fragment() {

    private lateinit var cacheUtil: CacheUtil
    private var profileUser: GetBalance.Data? = null
    private lateinit var dialogOrder: AlertDialog
    private var statusLoadData = false
    private lateinit var rootActivity: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rootActivity.setLoadedDataStatus(true)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(requireActivity(), getString(R.string.path_name))

        val checkAuth = this.cacheUtil.get(auth)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(requireContext(), SignInActivity::class.java))
            requireActivity().finish()
        }

        val profileAdapter = AdapterUtil(R.layout.item_profile, listOf(
            ItemProfile(R.drawable.ic_contact, "Kontak"),
            ItemProfile(R.drawable.ic_privation, "Kebijakan Privasi"),
            ItemProfile(R.drawable.ic_role, "Syarat dan Ketentuan"),
            ItemProfile(R.drawable.ic_star, "Beri Kami Nilai")
        ), { itemView, item ->
            itemView.item_profile_name.text = item.label
            itemView.item_profile_icon.setImageResource(item.icon)
            if (item.icon == R.drawable.ic_star)
                itemView.item_profile_version.text = StringBuilder()
                    .append("v")
                    .append(BuildConfig.VERSION_NAME)
        }, { position , item ->
            val intent = Intent(requireContext(), WebActivity::class.java)
            intent.putExtra("name", item.label)
            when (position) {
                0 -> intent.putExtra("url", "https://jaketbus.my.id/index.php/welcome/page/1")
                1 -> intent.putExtra("url", "https://jaketbus.my.id/index.php/welcome/page/2")
                else -> intent.putExtra("url", "https://jaketbus.my.id/index.php/welcome/page/3")
            }
            startActivity(intent)
        })
        list_profile.layoutManager = LinearLayoutManager(requireContext())
        list_profile.adapter = profileAdapter

        tv_name.text = "-"
        tv_desc.text = "-"
        Glide.with(requireContext())
            .load(R.drawable.ic_profile)
            .circleCrop()
            .into(img_profile)

        container_button.setOnClickListener {
            this.editProfileDialog()
        }
        img_profile.setOnClickListener {
            this.pickFromGallery()
        }
        btn_logout.setOnClickListener { logout() }

        // Load profile
        this.loadProfile()
    }

    private fun checkIsAlreadyLoaded() {
        if (!statusLoadData) rootActivity.setLoadedDataStatus(false)
    }

    private fun logout() {
        this.cacheUtil.clear()
        startActivity(Intent(requireContext(), CheckAlreadyAuthActivity::class.java))
        requireActivity().finish()
    }

    private fun loadProfile() {
        statusLoadData = true

        if (loader != null) {
            loader.visibility = View.VISIBLE
            container_profile.visibility = View.GONE
            btn_logout.visibility = View.GONE
        }

        balance(service!!, {
            if (it.code == 200) {
                // Setup Top Bar
                if (tv_name != null) {
                    tv_name.text = StringBuilder().append(it.data.first_name).append(" ")
                        .append(it.data.last_name)
                    Glide.with(requireContext())
                        .load(it.data.utility.face.uri)
                        .circleCrop()
                        .into(img_profile)

                    // Setup Wallet Bar
                    tv_desc.text = StringBuilder().append(it.data.phone_number).append("\n")
                        .append(it.data.email)

                    profileUser = it.data

                    rootActivity.updateProfileGlobal(it)
                }

            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            if (loader != null) {
                loader.visibility = View.GONE
                container_profile.visibility = View.VISIBLE
                btn_logout.visibility = View.VISIBLE
            }
            statusLoadData = false
            checkIsAlreadyLoaded()

        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

            if (loader != null) {
                loader.visibility = View.GONE
                container_profile.visibility = View.VISIBLE
                btn_logout.visibility = View.VISIBLE
            }
            statusLoadData = false
            checkIsAlreadyLoaded()
        })
    }

    private fun doEditProfile(frontName: String, backName: String, email: String, phone_number: String) {
        loader.visibility = View.VISIBLE
        container_profile.visibility = View.GONE
        btn_logout.visibility = View.GONE

        val data = JsonObject()
        data.addProperty("first_name", frontName)
        data.addProperty("last_name", backName)
        data.addProperty("email", email)
        data.addProperty("phone_number", "62${phone_number}")
        data.addProperty("level", profileUser!!.utility.level)

        statusLoadData = true

        editProfile(service!!, data, {

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE

            if (it.code == 200) {
                loadProfile()
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            statusLoadData = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            container_profile.visibility = View.VISIBLE
            btn_logout.visibility = View.VISIBLE

            statusLoadData = false
            checkIsAlreadyLoaded()
        })
    }

    private fun editProfileDialog() {
        profileUser?.let {
            // Setup Dialog Logout
            val builder = AlertDialog.Builder(requireContext())
            val dialogInflater = layoutInflater
            val dialogView = dialogInflater.inflate(R.layout.dialog_profile_edit, null)

            Glide.with(requireContext())
                .load(it.utility.face.uri)
                .circleCrop()
                .into(dialogView.dpe_face)

            dialogView.dpe_face.setOnClickListener {
                dialogOrder.dismiss()
                pickFromGallery()
            }

            dialogView.dpe_front_name.setText(it.first_name)
            dialogView.dpe_back_name.setText(it.last_name)
            dialogView.dpe_email.setText(it.email)
            dialogView.dpe_phone_number.setText(it.phone_number.substring(2))

            dialogView.dpe_btn_save.setOnClickListener {
                dialogOrder.dismiss()
                doEditProfile(dialogView.dpe_front_name.text.toString(),
                    dialogView.dpe_back_name.text.toString(),
                    dialogView.dpe_email.text.toString(),
                    dialogView.dpe_phone_number.text.toString())
            }

            builder.setView(dialogView)
            dialogOrder = builder.create()
            dialogOrder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialogOrder.show()
        }
    }

    private fun doEditPhotoProfile(file: File?) {
        statusLoadData = true
        file?.apply {
            if (loader != null) {
                loader.visibility = View.VISIBLE
                container_profile.visibility = View.GONE
                btn_logout.visibility = View.GONE
            }

            val requestBody = RequestBody.create("multipart".toMediaTypeOrNull(), this)
            val data = MultipartBody.Part.createFormData("face",this.name,requestBody)

            editPhotoProfile(service!!, data, {

                if (loader != null) {
                    loader.visibility = View.GONE
                    container_profile.visibility = View.VISIBLE
                    btn_logout.visibility = View.VISIBLE
                }
                statusLoadData = false
                checkIsAlreadyLoaded()

                if (it.code == 200) {
                    loadProfile()
                } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            }, {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                statusLoadData = false
                checkIsAlreadyLoaded()

                if (loader != null) {
                    loader.visibility = View.GONE
                    container_profile.visibility = View.VISIBLE
                    btn_logout.visibility = View.VISIBLE
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            val image = ImagePicker.getFirstImageOrNull(data)
            if (image != null) {
                var file = File(image.path)
                runBlocking {
                    val job = GlobalScope.launch {
                        file = Compressor.compress(requireContext(), file) {
                            this.quality(80)
                            this.format(Bitmap.CompressFormat.JPEG)
                            this.size(1024) // 2 MB
                        }
                    }
                    job.join()
                    doEditPhotoProfile(file)
                }
            } else {
                Toast.makeText(requireContext(), "Failed to load image", Toast.LENGTH_LONG).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun pickFromGallery() {
        ImagePicker.create(this) // Activity or Fragment
            .limit(1)
            .start()
    }

    companion object {

        @JvmStatic
        fun newInstance(rootActivity: HomeActivity) =
            ProfileFragment().apply {
                this.rootActivity = rootActivity
            }
    }
}