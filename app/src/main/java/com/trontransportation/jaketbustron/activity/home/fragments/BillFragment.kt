package com.trontransportation.jaketbustron.activity.home.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.orders.OrderDetailActivity
import com.trontransportation.jaketbustron.models.GetBookingTicketOrder
import com.trontransportation.jaketbustron.models.getTicketOrder
import com.trontransportation.jaketbustron.utilities.AdapterUtil
import com.trontransportation.jaketbustron.utilities.convertDateToIDFormat
import com.trontransportation.jaketbustron.utilities.selected_ticket_order
import com.trontransportation.jaketbustron.utilities.service
import kotlinx.android.synthetic.main.fragment_bill.*
import kotlinx.android.synthetic.main.item_ticket_order.view.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class BillFragment : Fragment() {
    private lateinit var listTicketAdapter: AdapterUtil<GetBookingTicketOrder.Data>
    private lateinit var rootActivity: HomeActivity
    private var isLoadedTicket = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bill, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rootActivity.setLoadedDataStatus(true)

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        listTicketAdapter = AdapterUtil(R.layout.item_ticket_order, listOf(),
            {itemView, item ->
                itemView.item_ticket_tv_id_ticket.text = StringBuilder()
                    .append(item.payment_status)
                when (item.payment_status) {
                    "paid" -> itemView.item_ticket_tv_id_ticket.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorGreen))
                    "unpaid" -> itemView.item_ticket_tv_id_ticket.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    else -> itemView.item_ticket_tv_id_ticket.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorRed))
                }
                itemView.item_ticket_tv_payment.text = if (item.payment_method == "balance") "Dompet Jak" else "Indomaret"
                itemView.item_ticket_tv_po_name.text = item.po_name
                itemView.item_ticket_tv_route_from.text = item.origin
                itemView.item_ticket_tv_time_go.text = item.time_of_departure
                itemView.item_ticket_tv_route_to.text = item.destionation
                itemView.item_ticket_tv_price.text = numberFormat.format(item.price)
                itemView.item_ticket_tv_type.text = item.category
                itemView.item_ticket_tv_passenger_name.text = item.passanger_name
                itemView.item_ticket_tv_seat_available.text = StringBuilder()
                    .append("Kursi-").append(item.seat_number)
                itemView.item_ticket_tv_date.text = convertDateToIDFormat(
                    SimpleDateFormat("yyyy-MM-dd",
                        Locale.getDefault()).parse(item.date_of_departure)!!)
            },
            {position, item ->
                if (item.payment_status == "unpaid") {
                    selected_ticket_order = item
                    startActivity(Intent(requireContext(), OrderDetailActivity::class.java))
                }
            })

        list_order.layoutManager = LinearLayoutManager(requireContext())
        list_order.adapter = listTicketAdapter
        list_order.isNestedScrollingEnabled = false

        // Load My Ticket
        this.loadMyTicket()
    }

    private fun loadMyTicket() {
        loader.visibility = View.VISIBLE
        list_order.visibility = View.GONE
        getTicketOrder(service!!,  {
            if (it.code == 200) {
                listTicketAdapter.data = it.data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_order.visibility = View.VISIBLE

            isLoadedTicket = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader.visibility = View.GONE
            list_order.visibility = View.VISIBLE

            isLoadedTicket = false
            checkIsAlreadyLoaded()
        })
    }

    private fun checkIsAlreadyLoaded() {
        if (!isLoadedTicket) rootActivity.setLoadedDataStatus(false)
    }

    companion object {
        @JvmStatic
        fun newInstance(rootActivity: HomeActivity) =
            BillFragment().apply {
                this.rootActivity = rootActivity
            }
    }
}