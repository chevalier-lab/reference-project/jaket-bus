package com.trontransportation.jaketbustron.activity.orders

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.orders.fragments.BuyFragment
import com.trontransportation.jaketbustron.models.balance
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_comfirmation_order_bus.*
import java.text.NumberFormat
import java.util.*

class ComfirmationOrderBusActivity : AppCompatActivity() {

    private lateinit var animationUp: Animation
    private lateinit var animationDown: Animation
    private var isShowSheet = false
    private lateinit var paymentAdapter: ArrayAdapter<String>
    private var currentBalance = -1L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comfirmation_order_bus)

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(
            Locale("in", "ID")
        )

        paymentAdapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice)
        paymentAdapter.add("Dompet Jak")
        paymentAdapter.add("Indomaret")

        // Setup Toolbar
        this.setupToolbar()

        // Setup Animation
        this.animationUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
        this.animationDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)

        // Setup Confirmation Order
        if (selected_order_bus.routeFrom != null && selected_order_bus.routeTo != null) {
            val routeFrom = selected_order_bus.routeFrom
            val routeTo = selected_order_bus.routeTo

            tv_route.text = StringBuilder().append(getRouteMinify(routeFrom!!.city))
                .append(" - ").append(getRouteMinify(routeTo!!.city))

            tv_route_from.text = routeFrom.city
            tv_route_to.text = routeTo.city
        }

        selected_order_bus.scheduleGo?.let {
            tv_schedule_go.text = StringBuilder().append(convertDateToIDFormat(it)).append("\n").append(
                selected_order_bus.ticket!!.time_of_departure)
        }

        selected_order_bus.seat.let {
            val seatString = StringBuilder()
            it.forEachIndexed { index, itemSeat  ->
                seatString.append(if (itemSeat.seat < 10) "0${itemSeat.seat}"
                else itemSeat.seat)
                    .append(if (index == it.size - 1) "" else ", ")
            }
            tv_chair.text = seatString
        }

        selected_order_bus.ticket?.let {
            tv_po_name.text = it.po_name
            tv_price_total.text = numberFormat.format(it.price)
            tv_type.text = it.type_of_bus
        }

        // Setup QR Code
        val qrPlainText = Gson().toJson(selected_order_bus)
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(qrPlainText, BarcodeFormat.QR_CODE,480,480)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            img_qr_code.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace();
        }

        setPaymentMethod()

        payment_method.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Pilih metode pembayaran")
            builder.setAdapter(paymentAdapter) { dialog, which ->
                if (which == 0) selected_order_bus.paymentMethod = "balance"
                else selected_order_bus.paymentMethod = "indomaret"
                setPaymentMethod()
                dialog.dismiss()
            }
            builder.create().show()
        }
    }

    override fun onResume() {
        getProfile()
        super.onResume()
    }

    private fun getProfile() {
        balance(service!!, {
            if (it.code == 200) {
                // Setup Wallet Bar
                payment_balance.text = StringBuilder()
                    .append("Saldo: ")
                    .append(currencyFormat(it.data.wallet.result.balance))
                currentBalance = it.data.wallet.result.balance

                btn_confirmation.setOnClickListener {
                    if (selected_order_bus.paymentMethod == "balance") {
                        if (currentBalance > -1L) {
                            if (currentBalance < (selected_order_bus.ticket!!.price * selected_order_bus.countOfChair)) {
                                Toast.makeText(this, "Saldo tidak mencukupi", Toast.LENGTH_LONG).show()
                                return@setOnClickListener
                            }
                        }
                    }
                    supportFragmentManager.beginTransaction().replace(R.id.sheet_content,
                        BuyFragment.newInstance(this@ComfirmationOrderBusActivity)).commit()
                    sheet_order.startAnimation(animationUp)
                    sheet_order.visibility = View.VISIBLE
                    isShowSheet = true
                }
            } else Toast.makeText(this@ComfirmationOrderBusActivity, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(this@ComfirmationOrderBusActivity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun setPaymentMethod() {
        if (selected_order_bus.paymentMethod == "balance") {
            payment_method.text = StringBuilder().append("Dompet Jak")
            payment_balance.visibility = View.VISIBLE
        }
        else {
            payment_method.text = StringBuilder().append("Indomaret")
            payment_balance.visibility = View.GONE
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Konfirmasi Pesanan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isShowSheet) {
            isShowSheet = false
            sheet_order.visibility = View.GONE
        } else {
            startActivity(
                Intent(
                    this@ComfirmationOrderBusActivity,
                    ChooseTicketBusActivity::class.java
                )
            )
            finish()
        }
    }
}