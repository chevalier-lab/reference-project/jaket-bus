package com.trontransportation.jaketbustron.activity.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.models.ItemNotification
import com.trontransportation.jaketbustron.models.getNotification
import com.trontransportation.jaketbustron.utilities.AdapterUtil
import com.trontransportation.jaketbustron.utilities.service
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationFragment : Fragment() {

    private lateinit var listNotificationAdapter: AdapterUtil<ItemNotification>
    private lateinit var rootActivity: HomeActivity
    private var isLoadedNotification = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup List Notification Adapter
        this.listNotificationAdapter = AdapterUtil(R.layout.item_notification, listOf(),
            {itemView, item ->
                itemView.item_notification_type.text = item.type
                itemView.item_notification_title.text = item.title
                itemView.item_notification_description.text = item.description
                itemView.item_notification_date.text = item.date
            },
            {_, _ ->})

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rootActivity.setLoadedDataStatus(true)

        // Setup List Notification
        list_notification.layoutManager = LinearLayoutManager(requireContext())
        list_notification.adapter = listNotificationAdapter

        loadNotification()
    }

    private fun checkIsAlreadyLoaded() {
        if (!isLoadedNotification) rootActivity.setLoadedDataStatus(false)
    }

    private fun loadNotification() {
        loader.visibility = View.VISIBLE
        list_notification.visibility = View.GONE

        isLoadedNotification = true

        getNotification(service!!, 0, {
            if (it.code == 200) {
                val data: MutableList<ItemNotification> = arrayListOf()
                it.data.forEach { item-> data.add(ItemNotification(
                    item.type,
                    item.title,
                    item.content,
                    item.created_at
                )) }
                listNotificationAdapter.data = data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            list_notification.visibility = View.VISIBLE

            isLoadedNotification = false
            checkIsAlreadyLoaded()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            list_notification.visibility = View.VISIBLE

            isLoadedNotification = false
            checkIsAlreadyLoaded()
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(rootActivity: HomeActivity) =
            NotificationFragment().apply {
                this.rootActivity = rootActivity
            }
    }
}