package com.trontransportation.jaketbustron.activity.orders

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.activity.orders.fragments.LoadRouteFragment
import com.trontransportation.jaketbustron.apis.ApiClient
import com.trontransportation.jaketbustron.apis.ApiInterface
import com.trontransportation.jaketbustron.models.*
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_order_bus.*
import kotlinx.android.synthetic.main.item_last_order.view.*
import kotlinx.android.synthetic.main.item_promo.view.*
import java.text.SimpleDateFormat
import java.util.*


class OrderBusActivity : AppCompatActivity() {

    private lateinit var lastOrderAdapter: AdapterUtil<ItemLastOrder>
    private lateinit var promoHomeAdapter: AdapterUtil<GetExperiences.Data>
    private var countOfChair = 1
    private var isShowSheet = false
    private var selectedSchedule = action_schedule_go
    private var selectedRouteFrom: ItemLoadRoute? = null
    private var selectedRouteTo: ItemLoadRoute? = null
    private var selectedDateGo: Date? = Date()
    private var selectedDateBack: Date? = null
    private lateinit var animationUp: Animation
    private lateinit var animationDown: Animation
    private lateinit var progressDialog: ProgressDialog
    private var isLoadedRoute = false

    private var data: MutableList<ItemLoadRoute> = arrayListOf()

    private fun loadRoute() {
        isLoadedRoute = true
        getRoute(service!!, {
            if (it.code == 200) {
                data = arrayListOf()
                if (it.data != null) {
                    if (it.data!!.status) {
                    it.data!!.data.forEachIndexed { index, item ->
                        data.add(ItemLoadRoute(item.id.toString(), item.province, item.city, item.region))
                        if (item.city.equals("Jakarta Timur", true))
                            setSelectedRouteItem(action_load_route_from, data[index]);
                    }

                } else Toast.makeText(this@OrderBusActivity, it.data!!.message, Toast.LENGTH_LONG).show()
                }
            } else Toast.makeText(this@OrderBusActivity, it.message, Toast.LENGTH_LONG).show()
            isLoadedRoute = false
        }, {
            Toast.makeText(this@OrderBusActivity, it, Toast.LENGTH_LONG).show()
            isLoadedRoute = false
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_bus)

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Mohon tunggu ...")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)

        // Setup Toolbar
        this.setupToolbar()

        loadRoute()

        // Setup Animation
        this.animationUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
        this.animationDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)

        // Setup Order Form
        tv_route_from.text = StringBuilder().append("-")
        tv_route_from_code.text = StringBuilder().append("-")
        tv_route_to.text = StringBuilder().append("-")
        tv_route_to_code.text = StringBuilder().append("-")
        tv_schedule_go.text = StringBuilder().append(convertDateToIDFormat(selectedDateGo!!))
        tv_count_of_chair.text = StringBuilder().append(countOfChair)

        // Check Before
        this.checkBefore()

        switch_schedule.setOnCheckedChangeListener { _, b ->
            if (b) {
                selectedDateBack = Date()
                tv_schedule_back.text = StringBuilder().append(convertDateToIDFormat(selectedDateBack!!))
                tv_schedule_back.visibility = View.VISIBLE
                tv_label_schedule_back.visibility = View.VISIBLE
            } else {
                selectedDateBack = null
                tv_schedule_back.visibility = View.GONE
                tv_label_schedule_back.visibility = View.GONE
            }
        }

        btn_min.setOnClickListener {
            if (countOfChair > 1) {
                countOfChair--
                tv_count_of_chair.text = StringBuilder().append(countOfChair)
            }
        }
        btn_max.setOnClickListener {
            countOfChair++
            tv_count_of_chair.text = StringBuilder().append(countOfChair)
        }

        // Setup Last Order
        lastOrderAdapter = AdapterUtil(R.layout.item_last_order, listOf(),
            {itemView, item ->
                itemView.item_last_order_route.text = StringBuilder()
                    .append(item.routeFrom).append(" - ").append(item.routeTo)
                itemView.item_last_order_date.text = item.orderDate
                itemView.item_last_order_count_of_chair.text = StringBuilder()
                    .append(item.countOfChair).append(" orang")
            },
            {_, _ ->})
        list_last_order.layoutManager = LinearLayoutManager(this@OrderBusActivity,
            LinearLayoutManager.HORIZONTAL, false)
        list_last_order.adapter = lastOrderAdapter

        promoHomeAdapter = AdapterUtil(R.layout.item_promo, listOf(),
            {itemView, item ->
                Glide.with(itemView)
                    .load(item.cover.uri)
                    .into(itemView.item_promo_img)
                itemView.item_promo_title.text = StringBuilder().append(item.title)
                itemView.item_promo_rating.text = StringBuilder().append(item.rating)
            },
            {_, _->})

        list_promo.layoutManager = LinearLayoutManager(this@OrderBusActivity,
        LinearLayoutManager.HORIZONTAL, false)
        list_promo.adapter = promoHomeAdapter

        // Handler From
        container_from.setOnClickListener {
            if (!isLoadedRoute) {
                isShowSheet = true
                showHideSheetFromTo()
                supportFragmentManager.beginTransaction().replace(
                    R.id.sheet_content,
                    LoadRouteFragment.newInstance(this@OrderBusActivity, action_load_route_from, data)
                ).commit()
            }
        }
        container_to.setOnClickListener {
            if (!isLoadedRoute) {
                isShowSheet = true
                showHideSheetFromTo()
                supportFragmentManager.beginTransaction().replace(R.id.sheet_content,
                    LoadRouteFragment.newInstance(this@OrderBusActivity, action_load_route_to, data)).commit()
            }
        }
        swap_route.setOnClickListener {
            if (selectedRouteFrom != null && selectedRouteTo != null) {
                val temp = selectedRouteFrom
                selectedRouteFrom = selectedRouteTo
                selectedRouteTo = temp
                tv_route_from.text = StringBuilder().append(selectedRouteFrom!!.province)
                tv_route_from_code.text = StringBuilder().append(getRouteMinify(selectedRouteFrom!!.city))
                tv_route_to.text = StringBuilder().append(selectedRouteTo!!.province)
                tv_route_to_code.text = StringBuilder().append(getRouteMinify(selectedRouteTo!!.city))
            }
        }

        // Handler Schedule
        container_schedule_go.setOnClickListener {
            selectedSchedule = action_schedule_go
            showDatePicker()
        }
        container_schedule_back.setOnClickListener {
            selectedSchedule = action_schedule_back
            showDatePicker()
        }

        // Load Order
        this.loadOrder()

        // Load Promo
        this.loadExperience()

        // Handler Order Bus
        btn_order_bus.isEnabled = true
        btn_order_bus.setBackgroundResource(R.drawable.background_circle_orange)
        btn_order_bus.setOnClickListener {
            when {
                selectedRouteTo == null -> Toast.makeText(applicationContext,
                    "Harap memilih rute tujuan terlebih dahulu", Toast.LENGTH_LONG).show()
                selectedRouteFrom == null -> Toast.makeText(applicationContext,
                    "Harap memilih rute asal terlebih dahulu", Toast.LENGTH_LONG).show()
                selectedDateGo == null -> Toast.makeText(applicationContext,
                    "Harap memilih tanggal keberangkatan terlebih dahulu", Toast.LENGTH_LONG).show()
                else -> {
                    selected_order_bus = OrderBus(selectedRouteFrom, selectedRouteTo,
                        selectedDateGo, selectedDateBack)
                    selected_order_bus.countOfChair = countOfChair

                    // Create Order
                    this.createOrder()
                }
            }
        }
    }

    private fun loadExperience() {
        getExperiences(service!!, 0, {
            if (it.code == 200) {
                promoHomeAdapter.data = it.data
            } else {
                Toast.makeText(this@OrderBusActivity, it.message, Toast.LENGTH_LONG).show()
            }
        }, {
            Toast.makeText(this@OrderBusActivity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun loadOrder() {
        progressDialog.show()

        getOrder(service!!, 0, {
            if (it.code == 200) {
                val data: MutableList<ItemLastOrder> = arrayListOf()
                it.data.forEach { item -> data.add(ItemLastOrder(
                    item.route_from.city,
                    item.route_to.city,
                    item.schedule_go.date,
                    item.count_of_passenger
                )) }
                lastOrderAdapter.data = data
            } else Toast.makeText(this@OrderBusActivity, it.message, Toast.LENGTH_LONG).show()
            progressDialog.dismiss()
        }, {
            Toast.makeText(this@OrderBusActivity, it, Toast.LENGTH_LONG).show()
            progressDialog.dismiss()
        })
    }

    private fun createOrder() {
        val order = CreateOrder(
            CreateOrder.Route(
                selectedRouteFrom!!.id.toLong(),
                selectedRouteFrom!!.province,
                selectedRouteFrom!!.city,
                selectedRouteFrom!!.region
            ),
            CreateOrder.Route(
                selectedRouteTo!!.id.toLong(),
                selectedRouteTo!!.province,
                selectedRouteTo!!.city,
                selectedRouteTo!!.region
            ),
            countOfChair,
            getDate(selectedDateGo!!)
        )
        val json = JsonParser().parse(Gson().toJson(order)).asJsonObject
        progressDialog.show()
        createOrderSchedule(service!!, json, {
            progressDialog.dismiss()
            if (it.code == 200) {
                create_order_id = it.data.id

                startActivity(Intent(this@OrderBusActivity, ChooseTicketBusActivity::class.java))
                finish()
            } else Toast.makeText(this@OrderBusActivity, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(this@OrderBusActivity, it, Toast.LENGTH_LONG).show()
            progressDialog.dismiss()
        })
    }

    private fun checkBefore() {
        selectedRouteFrom = selected_order_bus.routeFrom
        selectedRouteTo = selected_order_bus.routeTo
        selectedDateGo = selected_order_bus.scheduleGo
        selectedDateBack = selected_order_bus.scheduleBack

        if (selectedRouteFrom != null) {
            tv_route_from.text = StringBuilder().append(selectedRouteFrom!!.province)
            tv_route_from_code.text = StringBuilder().append(getRouteMinify(selectedRouteFrom!!.city))
        }
        if (selectedRouteTo != null) {
            tv_route_to.text = StringBuilder().append(selectedRouteTo!!.province)
            tv_route_to_code.text = StringBuilder().append(getRouteMinify(selectedRouteTo!!.city))
        }
        if (selectedDateGo != null) {
            tv_schedule_go.text = StringBuilder().append(convertDateToIDFormat(selectedDateGo!!))
        } else {
            selectedDateGo = Date()
            tv_schedule_go.text = StringBuilder().append(convertDateToIDFormat(selectedDateGo!!))
        }
    }

    private fun setSelectedRouteItem(action: String, data: ItemLoadRoute) {
        if (action == action_load_route_from) {
            selectedRouteFrom = data
            tv_route_from.text = StringBuilder().append(data.province)
            tv_route_from_code.text = StringBuilder().append(getRouteMinify(data.city))
        } else {
            selectedRouteTo = data
            tv_route_to.text = StringBuilder().append(data.province)
            tv_route_to_code.text = StringBuilder().append(getRouteMinify(data.city))
        }
    }

    fun setSelectedRoute(action: String, data: ItemLoadRoute) {
        onBackPressed()
        if (action == action_load_route_from) {
            selectedRouteFrom = data
            tv_route_from.text = StringBuilder().append(data.province)
            tv_route_from_code.text = StringBuilder().append(getRouteMinify(data.city))
        } else {
            selectedRouteTo = data
            tv_route_to.text = StringBuilder().append(data.province)
            tv_route_to_code.text = StringBuilder().append(getRouteMinify(data.city))
        }
    }

    private fun showDatePicker() {
        val date = Calendar.getInstance()
        val mDate = DatePickerDialog(this@OrderBusActivity, dateListener,
            date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
        mDate.datePicker.minDate = System.currentTimeMillis() - 1000
        mDate.show()
    }

    private val dateListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale("ID"))
        if (selectedSchedule == action_schedule_go) {
            selectedDateGo = formatter.parse("${year}-${(month + 1)}-${day}")
            tv_schedule_go.text = StringBuilder().append(convertDateToIDFormat(selectedDateGo!!))
        } else {
            selectedDateBack = formatter.parse("${year}-${(month + 1)}-${day}")
            tv_schedule_back.text = StringBuilder().append(convertDateToIDFormat(selectedDateBack!!))
        }
    }

    private fun showHideSheetFromTo() {
        if (isShowSheet) sheet_selected_from_to.visibility = View.VISIBLE
        else sheet_selected_from_to.visibility = View.GONE
        sheet_selected_from_to.startAnimation(animationUp)
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Order Bus"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isShowSheet) {
            isShowSheet = false
            sheet_selected_from_to.startAnimation(animationDown)
            sheet_selected_from_to.visibility = View.GONE
        } else {
            selected_order_bus = OrderBus()
            selected_seat = arrayListOf()
            startActivity(Intent(this@OrderBusActivity, HomeActivity::class.java))
            finish()
        }
    }
}