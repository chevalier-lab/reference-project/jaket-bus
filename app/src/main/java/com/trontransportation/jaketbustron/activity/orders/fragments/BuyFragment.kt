package com.trontransportation.jaketbustron.activity.orders.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.orders.ComfirmationOrderBusActivity
import com.trontransportation.jaketbustron.activity.orders.SuccessOrderBusActivity
import com.trontransportation.jaketbustron.models.*
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.fragment_buy.*

class BuyFragment : Fragment() {

    private lateinit var rootActivity: ComfirmationOrderBusActivity
    private var selectedGender: String = "m"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_buy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Disable Button Buy
        btn_buy.isEnabled = false

        // Watch Form
        radioM.setOnCheckedChangeListener { _, b ->
            if (b) selectedGender = "m"
        }
        radioF.setOnCheckedChangeListener { _, b ->
            if (b) selectedGender = "f"
        }
        tv_passenger_name.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                checkIsEnable()
            }
        })
        tv_phone_number.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length == 0) {return}

                p0?.apply {
                    if (this.isNotEmpty() && this[0] == '0') {
                        this.replace(0, 1, "")
                    }
                }

                checkIsEnable()
            }
        })

        btn_buy.text = getString(R.string.button_next)
        var passengerPosition = 0
        if (selected_order_bus.countOfChair == 1) btn_buy.text = getString(R.string.button_buy)
        btn_buy.setOnClickListener {
            when {
                TextUtils.isEmpty(tv_passenger_name.text.toString()) -> Toast.makeText(requireContext(),
                    "Harap mengisi nama penumpang", Toast.LENGTH_LONG).show()
                TextUtils.isEmpty(tv_phone_number.text.toString()) -> Toast.makeText(requireContext(),
                    "Harap mengisi nomor telepon penumpang", Toast.LENGTH_LONG).show()
                else -> {
                    if (selected_order_bus.passenger.size >= selected_order_bus.countOfChair) {
                        this.createOrderTicket()
                    } else {
                        selected_order_bus.passenger.add(ItemPassenger(
                            tv_passenger_name.text.toString(),
                            "62${tv_phone_number.text}",
                            selectedGender, selected_order_bus.seat[passengerPosition].id_seat))
                        tv_passenger_name.setText("")
                        tv_phone_number.setText("")
                        if (selected_order_bus.passenger.size >= selected_order_bus.countOfChair) {
                            this.createOrderTicket()
                        }
                        passengerPosition++
                        if (passengerPosition == (selected_order_bus.countOfChair - 1))
                            btn_buy.text = getString(R.string.button_buy)
                    }
                }
            }
        }
    }

    private fun createOrderTicket() {
        loader.visibility = View.VISIBLE
        btn_buy.visibility = View.GONE
        container_form.visibility = View.GONE

        val json = JsonObject()
        val datas = JsonArray()
        selected_order_bus.passenger.forEachIndexed { position, item ->
            val data = JsonObject()
            data.addProperty("id_schedule", selected_order_bus.ticket!!.id_schedule)
            data.addProperty("pnr", StringBuilder().append(item.name)
                .append(System.currentTimeMillis()).toString())
            data.addProperty("pnp_name", item.name)
            data.addProperty("pnp_no_telp", item.phone_number)
            data.addProperty("pnp_genre", item.gender)
            data.addProperty("id_seat", item.id_seat)
            data.addProperty("seat", selected_order_bus.seat[position].seat)
            datas.add(data)
        }

        json.add("booking", datas)
        json.addProperty("id_u_user_order", create_order_id)
        json.addProperty("id_schedule", selected_order_bus.ticket!!.id_schedule)
        json.addProperty("id_info", selected_order_bus.ticket!!.id_info)
        json.addProperty("date_of_departure", selected_order_bus.ticket!!.date_of_departure)
        json.addProperty("time_of_departure", selected_order_bus.ticket!!.time_of_departure)
        json.addProperty("id_of_origin", selected_order_bus.ticket!!.id_of_origin)
        json.addProperty("destionation_id", selected_order_bus.ticket!!.destionation_id)
        json.addProperty("id_category", selected_order_bus.ticket!!.id_category)
        json.addProperty("id_po", selected_order_bus.ticket!!.id_po)
        json.addProperty("total_passenger", selected_order_bus.countOfChair)
        json.addProperty("po_name", selected_order_bus.ticket!!.po_name)
        json.addProperty("destionation", selected_order_bus.ticket!!.destionation)
        json.addProperty("origin", selected_order_bus.ticket!!.origin)
        json.addProperty("type_of_bus", selected_order_bus.ticket!!.type_of_bus)
        json.addProperty("available", selected_order_bus.ticket!!.available)
        json.addProperty("price", selected_order_bus.ticket!!.price)
        json.addProperty("route", selected_order_bus.ticket!!.route)
        json.addProperty("payment_method", selected_order_bus.paymentMethod)

        val facilities = JsonArray()
        selected_order_bus.ticket!!.facility.forEach {
            val data = JsonObject()
            data.addProperty("facility_icon", it.facility_icon)
            data.addProperty("facility_name", it.facility_name)
            facilities.add(data)
        }
        json.add("facilities", facilities)

        val seats = JsonArray()
        selected_order_bus.seat.forEach {
            val data = JsonObject()
            data.addProperty("available", it.available)
            data.addProperty("id_item", it.id_item)
            data.addProperty("id_seat", it.id_seat)
            data.addProperty("item_icon", it.item_icon)
            data.addProperty("item_name", it.item_name)
            data.addProperty("number", it.number)
            data.addProperty("seat", it.seat)
            seats.add(data)
        }
        json.add("seats", seats)

        createOrderTicket(service!!, json, {

            loader.visibility = View.GONE
            btn_buy.visibility = View.VISIBLE
            container_form.visibility = View.VISIBLE

            if (it.code == 200) {
                selected_order_bus = OrderBus()
                selected_seat = arrayListOf()
                Toast.makeText(requireContext(), "Berhasil memesan tiket", Toast.LENGTH_LONG).show()
                startActivity(Intent(requireContext(), SuccessOrderBusActivity::class.java))
                requireActivity().finish()
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

            loader.visibility = View.GONE
            btn_buy.visibility = View.VISIBLE
            container_form.visibility = View.VISIBLE
        })
    }

//    private fun bookingTicket() {
//
//        loader.visibility = View.VISIBLE
//        btn_buy.visibility = View.GONE
//        container_form.visibility = View.GONE
//
//        val json = JsonObject()
//        val datas = JsonArray()
//        selected_order_bus.passenger.forEach { item ->
//            val data = JsonObject()
//            data.addProperty("id_schedule", selected_order_bus.ticket!!.id_schedule)
//            data.addProperty("pnr", StringBuilder().append(item.name)
//                .append(System.currentTimeMillis()).toString())
//            data.addProperty("pnp_name", item.name)
//            data.addProperty("pnp_no_telp", item.phone_number)
//            data.addProperty("pnp_genre", item.gender)
//            data.addProperty("id_seat", item.id_seat)
//            datas.add(data)
//        }
//
//        json.add("booking", datas)
//        json.addProperty("price", selected_order_bus.ticket!!.price)
//        json.addProperty("route", selected_order_bus.ticket!!.route)
//
//        createBooking(service!!, json, {
//
//            loader.visibility = View.GONE
//            btn_buy.visibility = View.VISIBLE
//            container_form.visibility = View.VISIBLE
//
//            if (it.code == 200) {
//                if (it.data.status) {
//                    selected_order_bus = OrderBus()
//                    selected_seat = arrayListOf()
//                    Toast.makeText(requireContext(), "Berhasil memesan tiket", Toast.LENGTH_LONG).show()
//                    startActivity(Intent(requireContext(), SuccessOrderBusActivity::class.java))
//                    requireActivity().finish()
//                } else Toast.makeText(requireContext(), it.data.message.joinToString(), Toast.LENGTH_LONG).show()
//            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
//        }, {
//            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
//
//            loader.visibility = View.GONE
//            btn_buy.visibility = View.VISIBLE
//            container_form.visibility = View.VISIBLE
//        })
//    }

    private fun checkIsEnable() {
        if (TextUtils.isEmpty(tv_passenger_name.text.toString()) || TextUtils
                .isEmpty(tv_phone_number.text.toString())) {
            btn_buy.isEnabled = false
            btn_buy.setBackgroundResource(R.drawable.background_cirlce_dark_gray)
            btn_buy.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorTextLight))
        } else {
            btn_buy.isEnabled = true
            btn_buy.setBackgroundResource(R.drawable.background_circle_orange)
            btn_buy.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: ComfirmationOrderBusActivity) =
            BuyFragment().apply {
                this.rootActivity = activity
            }
    }
}