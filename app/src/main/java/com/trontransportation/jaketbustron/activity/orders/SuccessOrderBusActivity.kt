package com.trontransportation.jaketbustron.activity.orders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import kotlinx.android.synthetic.main.activity_success_order_bus.*

class SuccessOrderBusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_order_bus)

        tv_message.text = StringBuilder().append("Berhasil memesan tiket")
        btn_finish.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this@SuccessOrderBusActivity, HomeActivity::class.java))
        finish()
    }
}