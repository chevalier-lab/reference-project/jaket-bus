package com.trontransportation.jaketbustron.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.trontransportation.jaketbustron.R
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val sesUrl = intent.getStringExtra("url")!!
        val sesName = intent.getStringExtra("name")!!

        web.settings.loadsImagesAutomatically = true
        web.settings.javaScriptEnabled = true
        web.settings.domStorageEnabled = true

        web.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        web.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.e("URL------------->",url.toString())
                val intent: Intent

                if (url!!.contains("gojek://gopay")) {
                    intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                    return true
                }
                return true
            }
        }
        web.loadUrl(sesUrl)
    }
}