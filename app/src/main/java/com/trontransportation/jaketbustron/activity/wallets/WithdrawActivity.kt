package com.trontransportation.jaketbustron.activity.wallets

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.R
import com.trontransportation.jaketbustron.activity.home.HomeActivity
import com.trontransportation.jaketbustron.models.WithdrawHistory
import com.trontransportation.jaketbustron.models.withdraw
import com.trontransportation.jaketbustron.models.withdrawHistory
import com.trontransportation.jaketbustron.utilities.*
import kotlinx.android.synthetic.main.activity_withdraw.*
import kotlinx.android.synthetic.main.item_withdraw.view.*

class WithdrawActivity : AppCompatActivity() {

    private var statusLoadData = false
    private lateinit var cashOutAdapter: AdapterUtil<WithdrawHistory.Data.Result.ItemWithdrawHistory>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_withdraw)

        // Setup Toolbar
        this.setupToolbar()

        cashOutAdapter = AdapterUtil(R.layout.item_withdraw, listOf(),
            { itemView, item ->
                itemView.item_withdraw_name.text = convertStringToIDFormat3(item.createdAt)
                itemView.item_withdraw_amount.text = currencyFormat(item.amount)
                itemView.item_withdraw_status.text = item.status

                when (item.status) {
                    "pending" -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_orange)
                    "success" -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_green)
                    else -> itemView.item_withdraw_status.setBackgroundResource(R.drawable.background_round_red)
                }
            },
            { _, _ -> })

        list_cash_out_history.layoutManager = LinearLayoutManager(this@WithdrawActivity)
        list_cash_out_history.adapter = cashOutAdapter

        btn_withdraw.setOnClickListener {
            if (TextUtils.isEmpty(edt_amount.text.toString())) {
                Toast.makeText(applicationContext, "Harap mengisi jumlah cash out", Toast.LENGTH_LONG).show()
            } else doWithDraw()
        }
    }

    override fun onResume() {
        loadWithDrawHistory()
        super.onResume()
    }

    private fun loadWithDrawHistory() {
        tv_heading_list.visibility = View.GONE
        list_cash_out_history.visibility = View.GONE
        loader.visibility = View.VISIBLE
        withdrawHistory(service!!, 0, {
            tv_heading_list.visibility = View.VISIBLE
            list_cash_out_history.visibility = View.VISIBLE
            loader.visibility = View.GONE

            if (it.code == 200) {
                if (it.data.success) {
                    cashOutAdapter.data = it.data.result.data
                } else Toast.makeText(applicationContext, it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
        }, {
            tv_heading_list.visibility = View.VISIBLE
            list_cash_out_history.visibility = View.VISIBLE
            loader.visibility = View.GONE

            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun doWithDraw() {
        loader_btn_withdraw.visibility = View.VISIBLE
        btn_withdraw.visibility = View.GONE
        val data = JsonObject()
        data.addProperty("amount", edt_amount.text.toString())
        withdraw(service!!, data, {
            loader_btn_withdraw.visibility = View.GONE
            btn_withdraw.visibility = View.VISIBLE
            if (it.code == 200) {
                if (it.data.success) {
                    edt_amount.setText("")
                    Toast.makeText(applicationContext, "Berhasil melakukan cash out", Toast.LENGTH_LONG).show()
                    loadWithDrawHistory()
                } else Toast.makeText(applicationContext, it.data.message, Toast.LENGTH_LONG).show()
            } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
        }, {
            loader_btn_withdraw.visibility = View.GONE
            btn_withdraw.visibility = View.VISIBLE
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Cash Out"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!statusLoadData) {
            startActivity(Intent(this@WithdrawActivity, HomeActivity::class.java))
            finish()
        } else {
            Toast.makeText(applicationContext, "Harap menunggu, kami sedang mempersiapkan data", Toast.LENGTH_LONG).show()
        }
    }
}