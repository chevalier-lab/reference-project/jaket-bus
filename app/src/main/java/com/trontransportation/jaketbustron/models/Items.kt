package com.trontransportation.jaketbustron.models

import java.util.*

data class ItemFeatureHome (
    var icon: Int,
    var label: Int
)

data class ItemLastOrder (
    var routeFrom: String,
    var routeTo: String,
    var orderDate: String,
    var countOfChair: Int
)

data class ItemChooseTicket (
    var id_schedule: Long,
    var id_info: Int,
    var date_of_departure: String,
    var time_of_departure: String,
    var price: Long,
    var id_of_origin: Int,
    var destionation_id: Int,
    var route: String,
    var id_category: Int,
    var id_po: String,
    var total_passenger: Int,
    var po_name: String,
    var destionation: String,
    var origin: String,
    var type_of_bus: String,
    var available: Int,
    var facility: MutableList<Facility>
) {
    data class Facility(
        var facility_name: String,
        var facility_icon: String
    )
}

data class ItemLoadRoute(
    var id: String,
    var province: String,
    var city: String,
    var region: String
)

data class ItemPassenger(
    var name: String,
    var phone_number: String,
    var gender: String,
    var id_seat: Long
)

data class ItemNotification(
    var type: String,
    var title: String,
    var description: String,
    var date: String
)

data class OrderBus(
    var routeFrom: ItemLoadRoute? = null,
    var routeTo: ItemLoadRoute? = null,
    var scheduleGo: Date? = null,
    var scheduleBack: Date? = null,
    var ticket: ItemChooseTicket? = null,
    var seat: MutableList<Seat.Data.ItemSeat> = arrayListOf(),
    var passenger: MutableList<ItemPassenger> = arrayListOf(),
    var countOfChair: Int = 1,
    var paymentMethod: String = "balance"
)

data class CreateUser(
    var phone_number: String = "",
    var email: String = "",
    var first_name: String = "",
    var last_name: String = "",
    var password: String = "",
    var level: Int = 0
)

data class CreateOrder(
    var route_from: Route? = null,
    var route_to: Route? = null,
    var count_of_passenger: Int = 1,
    var schedule_go: String? = ""
) {
    data class Route(
        var id_route: Long = 1,
        var province: String = "",
        var city: String = "",
        var region: String = ""
    )
}

data class ItemProfile(
    var icon: Int,
    var label: String
)