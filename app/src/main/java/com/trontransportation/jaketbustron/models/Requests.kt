package com.trontransportation.jaketbustron.models

import android.util.Log
import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.apis.ApiClient
import com.trontransportation.jaketbustron.apis.ApiInterface
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// Users ===========================================================================================
// Sign In
fun signIn(
    jsonObject: JsonObject,
    onSuccess: (SignIn) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<SignIn> = ApiClient.getClient.signIn(jsonObject)
    call.enqueue(object : Callback<SignIn> {
        override fun onFailure(call: Call<SignIn>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<SignIn>, response: Response<SignIn>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Sign Up
fun signUp(
    jsonObject: JsonObject,
    onSuccess: (SignUp) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<SignUp> = ApiClient.getClient.signUp(jsonObject)
    call.enqueue(object : Callback<SignUp> {
        override fun onFailure(call: Call<SignUp>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<SignUp>, response: Response<SignUp>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Edit Photo Profile
fun editProfile(
    service: ApiInterface,
    jsonObject: JsonObject,
    onSuccess: (EditProfile) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<EditProfile> = service.editProfile(jsonObject)
    call.enqueue(object : Callback<EditProfile> {
        override fun onFailure(call: Call<EditProfile>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Edit Photo Profile
fun editPhotoProfile(
    service: ApiInterface,
    data: MultipartBody.Part,
    onSuccess: (EditPhotoProfile) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<EditPhotoProfile> = service.editPhotoProfile(data)
    call.enqueue(object : Callback<EditPhotoProfile> {
        override fun onFailure(call: Call<EditPhotoProfile>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<EditPhotoProfile>, response: Response<EditPhotoProfile>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Wallets =========================================================================================
// Balance
fun balance(
    service: ApiInterface,
    onSuccess: (GetBalance) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetBalance> = service.balance()
    call.enqueue(object : Callback<GetBalance> {
        override fun onFailure(call: Call<GetBalance>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetBalance>, response: Response<GetBalance>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// TOp Up
fun topUp(
    service: ApiInterface,
    data: JsonObject,
    onSuccess: (Deposit) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Deposit> = service.deposit(data)
    call.enqueue(object : Callback<Deposit> {
        override fun onFailure(call: Call<Deposit>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Deposit>, response: Response<Deposit>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// TOp Up
fun topUpList(
    service: ApiInterface,
    page: Int,
    onSuccess: (DepositHistory) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<DepositHistory> = service.depositHistory(page)
    call.enqueue(object : Callback<DepositHistory> {
        override fun onFailure(call: Call<DepositHistory>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<DepositHistory>, response: Response<DepositHistory>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Withdraw
fun withdraw(
    service: ApiInterface,
    data: JsonObject,
    onSuccess: (Withdraw) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Withdraw> = service.withdraw(data)
    call.enqueue(object : Callback<Withdraw> {
        override fun onFailure(call: Call<Withdraw>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Withdraw>, response: Response<Withdraw>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Withdraw History
fun withdrawHistory(
    service: ApiInterface,
    page: Int,
    onSuccess: (WithdrawHistory) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<WithdrawHistory> = service.withdrawHistory(page)
    call.enqueue(object : Callback<WithdrawHistory> {
        override fun onFailure(call: Call<WithdrawHistory>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<WithdrawHistory>, response: Response<WithdrawHistory>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Order Bus =======================================================================================
// Route
fun getRoute(
    service: ApiInterface,
    onSuccess: (GetRoute) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetRoute> = service.route()
    call.enqueue(object : Callback<GetRoute> {
        override fun onFailure(call: Call<GetRoute>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetRoute>, response: Response<GetRoute>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Create Order
fun createOrderSchedule(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (InsertOrder) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<InsertOrder> = service.createOrder(json)
    call.enqueue(object : Callback<InsertOrder> {
        override fun onFailure(call: Call<InsertOrder>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<InsertOrder>, response: Response<InsertOrder>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Schedule
fun getSchedule(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (GetSchedule) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetSchedule> = service.schedule(json)
    call.enqueue(object : Callback<GetSchedule> {
        override fun onFailure(call: Call<GetSchedule>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetSchedule>, response: Response<GetSchedule>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Seats
fun getSeats(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (GetSeats) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetSeats> = service.seat(json)
    call.enqueue(object : Callback<GetSeats> {
        override fun onFailure(call: Call<GetSeats>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetSeats>, response: Response<GetSeats>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Create Order Ticket
fun createOrderTicket(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (InsertOrderTicket) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<InsertOrderTicket> = service.createOrderTicket(json)
    call.enqueue(object : Callback<InsertOrderTicket> {
        override fun onFailure(call: Call<InsertOrderTicket>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<InsertOrderTicket>, response: Response<InsertOrderTicket>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Booking Ticket
fun createBooking(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (Booking) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Booking> = service.booking(json)
    call.enqueue(object : Callback<Booking> {
        override fun onFailure(call: Call<Booking>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Booking>, response: Response<Booking>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Ticket
fun getTicket(
    service: ApiInterface,
    json: JsonObject,
    onSuccess: (GetBookingTicket) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetBookingTicket> = service.getBooking(json)
    call.enqueue(object : Callback<GetBookingTicket> {
        override fun onFailure(call: Call<GetBookingTicket>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetBookingTicket>, response: Response<GetBookingTicket>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Ticket
fun getTicketOrder(
    service: ApiInterface,
    onSuccess: (GetBookingTicketOrder) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetBookingTicketOrder> = service.getBookingOrder()
    call.enqueue(object : Callback<GetBookingTicketOrder> {
        override fun onFailure(call: Call<GetBookingTicketOrder>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetBookingTicketOrder>, response: Response<GetBookingTicketOrder>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Order
fun getOrder(
    service: ApiInterface,
    page: Int,
    onSuccess: (GetOrder) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetOrder> = service.getOrder(page)
    call.enqueue(object : Callback<GetOrder> {
        override fun onFailure(call: Call<GetOrder>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetOrder>, response: Response<GetOrder>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Notification
fun getNotification(
    service: ApiInterface,
    page: Int,
    onSuccess: (GetNotification) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetNotification> = service.getNotification(page)
    call.enqueue(object : Callback<GetNotification> {
        override fun onFailure(call: Call<GetNotification>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetNotification>, response: Response<GetNotification>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Features
fun getFeatures(
    service: ApiInterface,
    page: Int,
    onSuccess: (GetFeatures) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetFeatures> = service.getFeatures(page)
    call.enqueue(object : Callback<GetFeatures> {
        override fun onFailure(call: Call<GetFeatures>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetFeatures>, response: Response<GetFeatures>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Experiences
fun getExperiences(
    service: ApiInterface,
    page: Int,
    onSuccess: (GetExperiences) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<GetExperiences> = service.getExperiences(page)
    call.enqueue(object : Callback<GetExperiences> {
        override fun onFailure(call: Call<GetExperiences>, t: Throwable) {
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<GetExperiences>, response: Response<GetExperiences>) {
            Log.d("REQUEST_BASIC_AUTH", response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}