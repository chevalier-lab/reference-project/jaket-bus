package com.trontransportation.jaketbustron.models

// USERS ===========================================================================================
data class Auth(
    var first_name: String,
    var last_name: String,
    var email: String,
    var password: String,
    var phone_number: String,
    var level: Int,
    var token: String
)
// Sign IN
data class SignIn(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Auth
)

// Sign UP
data class SignUp(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Auth
)

// Profile
data class Profile(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var first_name: String,
        var last_name: String,
        var email: String,
        var password: String,
        var phone_number: String,
        var utility: Utility
    ) {
        data class Utility(
            var level: Int,
            var token: String,
            var face: Face
        ) {
            data class Face(
                var uri: String,
                var label: String
            )
        }
    }
}

// Profile
data class EditProfile(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var first_name: String,
        var last_name: String,
        var level: Int,
        var token: String
    )
}

// Photo Profile
data class EditPhotoProfile(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var file_name: String,
        var file_location: String,
        var status: Boolean
    )
}

// WALLETS =========================================================================================
data class GetBalance(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var first_name: String,
        var last_name: String,
        var email: String,
        var password: String,
        var phone_number: String,
        var utility: Utility,
        var wallet: Wallet
    ) {
        data class Utility(
            var level: Int,
            var token: String,
            var face: Face
        ) {
            data class Face(
                var uri: String,
                var label: String
            )
        }

        data class Wallet(
            var success: Boolean,
            var result: Result
        ) {
            data class Result(
                var id: String,
                var name: String,
                var phone: String,
                var email: String,
                var balance: Long
            )
        }
    }
}

// Do Deposit
data class Deposit(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var success: Boolean,
        var message: String = "",
        var result: Result? = null
    ) {
        data class Result(
            var token: String,
            var redirect_url: String?
        )
    }
}

// Do Deposit
data class DepositHistory(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var success: Boolean,
        var message: String,
        var result: Result
    ) {
        data class Result(
            var page: Int,
            var data: MutableList<ItemDepositHistory>,
            var totalPage: Int,
            var total: Int
        ) {
            data class ItemDepositHistory(
                var createdAt: String,
                var id: Long,
                var amount: Long,
                var amount_uniq: Long,
                var payment_name: String,
                var status: String
            )
        }
    }
}

// Do Withdraw
data class Withdraw(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var success: Boolean,
        var message: String
    )
}

// Do Withdraw
data class WithdrawHistory(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var success: Boolean,
        var message: String,
        var result: Result
    ) {
        data class Result(
            var page: Int,
            var data: MutableList<ItemWithdrawHistory>,
            var totalPage: Int,
            var total: Int
        ) {
            data class ItemWithdrawHistory(
                var createdAt: String,
                var id: Long,
                var amount: Long,
                var phone: String,
                var status: String
            )
        }
    }
}

// ORDER BUS =======================================================================================
// Route
data class GetRoute(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data?
) {
    data class Data(
        var status: Boolean,
        var data: MutableList<DataItem>,
        var message: String = ""
    ) {
        data class DataItem(
            var id: Long,
            var province: String,
            var city: String,
            var region: String
        )
    }
}

// Create Order
data class InsertOrder(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var id_m_users: Long,
        var count_of_passenger: Int,
        var id: Long
    )
}

// Get Order
data class GetOrder(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var count_of_passenger: Int,
        var is_go_back: Int,
        var status: Int,
        var id_m_users: Long,
        var route_from: CreateOrder.Route,
        var route_to: CreateOrder.Route,
        var schedule_go: Schedule
    ) {
        data class Schedule(
            var id_u_user_order: Long,
            var date: String
        )
    }
}

// Get Order
data class GetNotification(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var type: String,
        var title: String,
        var content: String,
        var id_m_users: Long,
        var created_at: String,
        var updated_at: String
    )
}

// Schedule
data class GetSchedule(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var status: Boolean,
        var count: Long,
        var data: MutableList<DataItem> = arrayListOf(),
        var message: String = ""
    ) {
        data class DataItem(
            var id_schedule: Long,
            var id_info: Int,
            var date_of_departure: String,
            var time_of_departure: String,
            var price: Long,
            var id_of_origin: Int,
            var destionation_id: Int,
            var route: String,
            var id_category: Int,
            var id_po: String,
            var total_passenger: Int,
            var po_name: String,
            var destionation: String,
            var origin: String,
            var category: String,
            var available: Int,
            var facility: MutableList<Facility>
        ) {
            data class Facility(
                var facility_name: String,
                var facility_icon: String
            )
        }
    }
}

// Get Seats
data class GetSeats(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Seat
)

// Create Order Ticket
data class InsertOrderTicket(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = ""
//    var data: Data
) {
//    data class Data(
//        var id_u_user_order: Long,
//        var id_schedule: Long,
//        var id_info: Int,
//        var date_of_departure: String,
//        var time_of_departure: String,
//        var price: Long,
//        var id_of_origin: String,
//        var destionation_id: String,
//        var route: String,
//        var id_category: Int,
//        var id_po: String,
//        var total_passenger: Int,
//        var po_name: String,
//        var destionation: String,
//        var origin: String,
//        var type_of_bus: String,
//        var available: Int,
//        var id: Long
//    )
}

// Booking
data class Booking(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: BookingTicket
)

// Ticket
data class Ticket(
    var status: Boolean,
    var count: Int,
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id_schedule: Long,
        var id_info: Int,
        var date_of_departure: String,
        var time_of_departure: String,
        var price: Long,
        var id_of_origin: Int,
        var destionation_id: Int,
        var route: String,
        var id_category: Int,
        var id_po: String,
        var total_passenger: Int,
        var po_name: String,
        var destionation: String,
        var origin: String,
        var category: String,
        var available: Int,
        var facility: MutableList<Facility>
    ) {
        data class Facility(
            var facility_name: String,
            var facility_icon: String
        )
    }
}

// Seat
data class Seat(
    var status: Boolean,
    var data: Data,
    var message: String
) {
    data class Data(
        var id: Long,
        var pnp: Int,
        var layout: Layout,
        var seat: MutableList<ItemSeat>
    ) {
        data class Layout(
            var layout_id: Int,
            var layout_format: String,
            var seat_column: Int,
            var seat_row: Int
        )

        data class ItemSeat(
            var id_seat: Long,
            var number: Int,
            var seat: Int,
            var id_item: Int,
            var item_name: String? = "",
            var item_icon: String? = "",
            var available: Boolean
        )
    }
}

// Booking Ticket
data class BookingTicket(
    var status: Boolean,
    var message: MutableList<String> = arrayListOf()
)

// Booking Ticket
data class GetBookingTicket(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data> = arrayListOf()
) {
    data class Data(
        var id: Long,
        var id_m_users: Long,
        var ticket_id: Long,
        var passanger_name: String,
        var passanger_gender: String,
        var passanger_telp: String,
        var pnr: String,
        var seat_id: Long,
        var seat_number: Int,
        var is_boarding: Int,
        var date_of_boarding: String?,
        var date_of_departure: String,
        var time_of_departure: String,
        var price: Long,
        var id_schedule: Long,
        var id_of_origin: Long,
        var destionation_id: Long,
        var category: String,
        var po_name: String,
        var destionation: String,
        var origin: String,
        var facility: MutableList<Ticket.Data.Facility>
    )
}

// Booking Ticket
data class GetBookingTicketOrder(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data> = arrayListOf()
) {
    data class Data(
        var id: Long,
        var id_m_users: Long,
        var ticket_id: Long,
        var passanger_name: String,
        var passanger_gender: String,
        var passanger_telp: String,
        var pnr: String,
        var seat_id: Long,
        var seat_number: Int,
        var is_boarding: Int,
        var date_of_boarding: String?,
        var date_of_departure: String,
        var time_of_departure: String,
        var price: Long,
        var id_schedule: Long,
        var id_of_origin: Long,
        var destionation_id: Long,
        var category: String,
        var po_name: String,
        var destionation: String,
        var origin: String,
        var payment_status: String,
        var payment_method: String,
        var payment_fee: String,
        var payment_expired: String,
        var payment_code: String
    )
}

// Get Order
data class GetFeatures(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var title: String,
        var content: String,
        var id_m_users: Long,
        var id_m_medias: Long,
        var created_at: String,
        var updated_at: String,
        var cover: Cover
    ) {
        data class Cover(
            var id: Long,
            var uri: String,
            var label: String
        )
    }
}

// Get Experience
data class GetExperiences(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var title: String,
        var content: String,
        var rating: Double,
        var id_m_users: Long,
        var id_m_medias: Long,
        var created_at: String,
        var updated_at: String,
        var cover: Cover
    ) {
        data class Cover(
            var id: Long,
            var uri: String,
            var label: String
        )
    }
}