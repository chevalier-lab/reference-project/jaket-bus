package com.trontransportation.jaketbustron.apis

import com.google.gson.JsonObject
import com.trontransportation.jaketbustron.models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    // General ==================================================================
    // Sign In
    @POST("user/login")
    fun signIn(@Body jsonObject: JsonObject): Call<SignIn>

    // Sign Up
    @POST("user/registration")
    fun signUp(@Body jsonObject: JsonObject): Call<SignUp>

    // Profile
    @GET("user/profile")
    fun profile(): Call<Profile>

    // Edit Profile
    @POST("user/update_profile")
    fun editProfile(@Body jsonObject: JsonObject): Call<EditProfile>

    // Update Photo Profile
    @Multipart
    @POST("user/update_photo_profile")
    fun editPhotoProfile(@Part body: MultipartBody.Part): Call<EditPhotoProfile>

    // Wallet ==================================================================
    // Balance
    @GET("tronWallet/getBalance")
    fun balance(): Call<GetBalance>

    // Balance
    @POST("tronWallet/deposito")
    fun deposit(@Body jsonObject: JsonObject): Call<Deposit>

    // Balance
    @POST("tronWallet/historyDeposito")
    fun depositHistory(@Query("page") page: Int): Call<DepositHistory>

    // Balance
    @POST("tronWallet/withdraw")
    fun withdraw(@Body jsonObject: JsonObject): Call<Withdraw>

    // Balance
    @POST("tronWallet/historyWithdraw")
    fun withdrawHistory(@Query("page") page: Int): Call<WithdrawHistory>

    // Order Bus ==================================================================
    // Route
    @GET("puloGebang/getRoute")
    fun route(): Call<GetRoute>

    // Create Order
    @POST("orders/createOrder")
    fun createOrder(@Body jsonObject: JsonObject): Call<InsertOrder>

    // Schedule
    @POST("puloGebang/getSchedule")
    fun schedule(@Body jsonObject: JsonObject): Call<GetSchedule>

    // Seats
    @POST("puloGebang/getSeat")
    fun seat(@Body jsonObject: JsonObject): Call<GetSeats>

    // Create Order Ticket
    @POST("orders/createOrderTicket")
    fun createOrderTicket(@Body jsonObject: JsonObject): Call<InsertOrderTicket>

    // Create Order Ticket
    @POST("puloGebang/booking")
    fun booking(@Body jsonObject: JsonObject): Call<Booking>

    // Get Booking
    @POST("orders/getTickets")
    fun getBooking(@Body jsonObject: JsonObject): Call<GetBookingTicket>

    // Get History Order
    @GET("orders/ticket_unpaid_user")
    fun getBookingOrder(): Call<GetBookingTicketOrder>

    // Get Order
    @POST("orders/getOrder")
    fun getOrder(@Query("page") page: Int): Call<GetOrder>

    // Get Notification
    @POST("notification/getNotification")
    fun getNotification(@Query("page") page: Int): Call<GetNotification>

    // Get Features
    @POST("features/getFeatures")
    fun getFeatures(@Query("page") page: Int): Call<GetFeatures>

    // Get Experience
    @POST("experiences/getExperiences")
    fun getExperiences(@Query("page") page: Int): Call<GetExperiences>
}

