package com.trontransportation.jaketbustron.utilities

import com.google.gson.Gson
import com.trontransportation.jaketbustron.apis.ApiClient
import com.trontransportation.jaketbustron.apis.ApiInterface
import com.trontransportation.jaketbustron.models.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

var before_otp = 0
var auth = "AUTHENTICATION_JAKET_BUS"
var authBefore = "AUTHENTICATION_JAKET_BUS_BEFORE"

var action_load_route_from = "FROM"
var action_load_route_to = "TO"

var action_schedule_go = "GO"
var action_schedule_back = "BACK"

var selected_order_bus = OrderBus()
var selected_seat: MutableList<Seat.Data.ItemSeat> = arrayListOf()

var create_user = CreateUser()
var create_order_id: Long = 0
var phone_number_otp = ""

var selected_ticket : GetBookingTicket.Data? = null
var selected_ticket_order : GetBookingTicketOrder.Data? = null

var service: ApiInterface? = null

fun createService(cacheUtil: CacheUtil) {
    if (service == null) {
        service = ApiClient.createService(getAuth(cacheUtil).token)
    }
}

fun getRouteMinify(words: String): String {
    val arr = words.split(" ")
    return if (arr.size > 1) StringBuilder().append(arr[0]).append(arr[1].take(3)).toString()
    else arr[0]
}

fun convertDateToIDFormat(date: Date): String {
    val formatter = SimpleDateFormat("EEEE, dd MMM yyyy", Locale("ID"))
    return formatter.format(date)
}

fun convertStringToIDFormat(date: String): String {
    val formatterDate = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
    val newDate = formatterDate.parse(date)
    val formatter = SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss", Locale("ID"))
    return formatter.format(newDate!!)
}

fun convertStringToIDFormat2(date: String): String {
    val formatterDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val newDate = formatterDate.parse(date)
    val formatter = SimpleDateFormat("EEEE, dd MMM yyyy", Locale("ID"))
    return formatter.format(newDate!!)
}

fun convertStringToIDFormat3(date: String): String {
    val formatterDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val newDate = formatterDate.parse(date)
    val formatter = SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss", Locale("ID"))
    return formatter.format(newDate!!)
}

fun getDate(date: Date): String {
    val formatter = SimpleDateFormat("yyyy-MM-dd ", Locale("ID"))
    return formatter.format(date)
}

fun getAuth(cacheUtil: CacheUtil): Auth =
    Gson().fromJson(cacheUtil.get(auth), Auth::class.java)

fun getAuthBeofre(cacheUtil: CacheUtil): Auth =
    Gson().fromJson(cacheUtil.get(authBefore), Auth::class.java)

fun currencyFormat(price: Long): String = NumberFormat.getCurrencyInstance(
    Locale("in", "ID")
).format(price).substring(2)