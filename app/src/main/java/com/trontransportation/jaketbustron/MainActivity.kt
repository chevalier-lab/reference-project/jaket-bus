package com.trontransportation.jaketbustron

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.trontransportation.jaketbustron.activity.CheckAlreadyAuthActivity
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timer("SettingUp", false).schedule(2000) {

            startActivity(Intent(this@MainActivity, CheckAlreadyAuthActivity::class.java))
            finish()

        }
    }
}